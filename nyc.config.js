module.exports = {
    "check-coverage": true,
    "lines": 100,
    "statements": 100,
    "functions": 100,
    "branches": 100,

    "extends": "@istanbuljs/nyc-config-typescript",
    "sourceMap": true,

    "temp-dir": "build-output/nyc-output",
    "report-dir": "build-output/coverage",

    "reporter": [
        "lcov",
        "text",
        "html"
    ],
}
