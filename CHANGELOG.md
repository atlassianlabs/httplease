# 0.10.0
- Only support Node v16 and higher
- bufferJsonResponseHandler won't try to parse the body if statusCode==204 or Content-Length==0.
- discardBodyResponseHandler now sets body=undefined
- All errors and types are exported directly on the httplease module.
- Wrap all unknown errors in `UnknownHttpleaseError`, the cause is available as `error.cause`.
  This is a breaking change if you're catching one of these errors. You'll need to handle this new wrapper Error instead.

# 0.9.0
- This version introduced error wrapping, but it mistakenly wrapped errors thrown by filters.

# 0.8.5
- Update defaultErrorResponseHandler to handle malformed JSON without throwing

# 0.8.4
- Update Builder types to better support mixins/inheritance

# 0.8.3
- Change return type of `Builder.send()` back to `any`

# 0.8.2
- Fix for filters being undefined.
- Improve type signature of `withConfig()` and `builder()`.

# 0.8.0
- Improvements to type definitions.
    - Return types of `Builder` methods are now `this` instead of `Builder`.
    - Optional arguments of `Builder` methods are now actually marked as optional.
    - Added more type annotations to `Builder` methods.

## Upgrading from 0.7.x
- Some `Builder` method arguments are now more strictly typed. This may be a breaking change but only affects TypeScript consumers.
    - `new Builder({ unrecognisedConfig: ... })` or `builder.send({ unrecognisedConfig: ... })`
        - If you need to do this (e.g. for passing config to filters or mixins), use a type assertion: `new Builder({ unrecognisedConfig: ... } as any)`.
        - This will be improved in a future version. 🙂
    - `builder.withClientName(123)` or `builder.withTimeout("123")`
        - Don't do this. Client name should be a string and timeout should be a number.

# Upgrading from 0.6.0 to 0.7.0:
- The only change is to fix a regression where the default buffer/json response handles did not have a default maximum buffer size.

# Upgrading from 0.5.x to 0.6.0:
- httplease is now written in TypeScript, some internals were moved around but this should not affect the public interface.
    - The type definitions are still fairly new and are likely to change incompatibly in the future.
- All errors are now typed, see `src/errors.ts`
- The private promise indirection has been removed, now `global.Promise` is always used.
