# httplease

Features:

* Promise based API and internals.
* Builder interface allowing partial configuration and reuse.
* Convenient defaults for working with JSON requests and responses.
* Safe use of streams with robust error handling.
* Flexibility to stream or buffer any type of requests, responses or both.
* Supports chainable filters which can asynchronously modify requests and/or responses.

Existing filters:

* [httplease-asap](https://bitbucket.org/atlassianlabs/httplease-asap) -- [ASAP S2S Authentication](http://s2sauth.bitbucket.org)
* [httplease-cache](https://bitbucket.org/atlassianlabs/httplease-cache) -- HTTP caching using the Cache-Control headers
* [httplease-retry](https://bitbucket.org/atlassianlabs/httplease-retry) -- Automatic request retries with optional fallback to alternative servers

Filters that are coming soon!

* Zipkin tracing

Possible future filters:

* Redirects
* Cookie jar

# Usage guide

Install the library:

```
npm install --save httplease
```

For more examples have a look at the `test/integration` directory.


## Simple request

```
const httplease = require('httplease');


// this can be saved and reused as many times as you want
const httpClient = httplease.builder()
    .withBaseUrl('http://example.com/basePath')
    .withExpectStatus([200, 204])
    .withBufferJsonResponseHandler()
    .withTimeout(10000)
    .withAgentOptions({keepAlive: true});


// make a request
httpClient
    .withPath('/post')
    .withJsonBody({someValue: 'some value'})
    .withHeaders({'Cookie': 'key=value'})
    .withMethodPost()
    .send()
    .then((response) => {
        console.log(response.statusCode);
        console.log('get header', response.getHeader('Some-Header'));
        console.log('all headers', response.headers);
        console.log('JSON body parsing by default', response.body);
    })
    .catch((err) => {
        if (err instanceof httplease.errors.UnexpectedHttpResponseCodeError) {
            // the response status check failed
            console.log(err.response.statusCode);
            console.log(err.response.headers);
            console.log(err.response.body);
        }

        // some unknown error
        throw err;
    });
```


## Passing parameters in the send() function

You can mix/match the builder style with params in `send()`

```
httplease.builder()
    .withBufferJsonResponseHandler()
    .withExpectStatus([200])
    .send({
        baseUrl: 'http://example.com/resource',
        method: 'POST',
        headers: {'Cookie': 'key=value'}
    })
    .then((response) => {
        console.log('response complete');
    });
```


## Streaming requests and responses

```
httplease.builder()
    .withMethodPost()
    .withPath('http://example.com/upload-file')
    .withHeaders({'Content-Type': 'application/json'})
    .withStreamBody(fs.createReadStream('bigdata.json'))
    .withDiscardBodyResponseHandler()
    .withExpectStatus([200])
    .send()
    .then((response) => {
        console.log('upload complete!');
    });
```

```
httplease.builder()
    .withMethodGet()
    .withPath('http://example.com/')
    .withExpectStatus([200])
    .withTimeout(1000)
    .withResponseHandler((response) => response.pipe(process.stdout))
    .send()
    .then((response) => {
        console.log('download complete!');
    });
```


# Development guide

## Install dependencies

```
npm install
```


## Useful commands

```
# Run all checks
npm test

# Run just the jasmine tests
npm run test:jasmine

# Run just the linter
npm run test:lint
```


## Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# License

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
