'use strict';

const describeIntegrationSupport = require('../support/describeIntegrationSupport');
const {ConnectTimeoutError} = require('../../lib/errors');
const _ = require('lodash');
const assert = require('assert');

const sleep = require('../support/sleep');

describeIntegrationSupport('keepAlive', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;

        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });
    });

    it('should keepAlive when enabled', () => {
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withAgentOptions({keepAlive: true})
            .send()
            .then(() => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                const args = httpSpy.getHandler.calls.argsFor(0)[0];
                expect(args.headers['connection']).toBe('keep-alive');
            });
    });

    it('should not keepAlive when disabled', () => {
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withAgentOptions({keepAlive: false})
            .send()
            .then(() => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                const args = httpSpy.getHandler.calls.argsFor(0)[0];
                expect(args.headers['connection']).toBe('close');
            });
    });

    it('should send Connection:keep-alive when using configured http.Agent', () => {
        const httpClientWithAgent = httpClient.withAgentOptions({keepAlive: true});

        return httpClientWithAgent
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .send()
            .then(() => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                const args = httpSpy.getHandler.calls.argsFor(0)[0];
                expect(args.headers['connection']).toBe('keep-alive');
                httpClientWithAgent.config.httpOptions.agent.destroy();
            });
    });

    describe('connectTimeouts', () => {
        let httpAgent;

        function getOnlyFreeSocket() {
            const sockets = [].concat(..._.values(httpAgent.freeSockets));
            assert.ok(sockets.length === 1);
            return sockets[0];
        }

        beforeEach(() => {
            httpClient = support.httpClient
                .withMethodGet()
                .withPath('/get')
                .withExpectStatus([200])
                .withBufferJsonResponseHandler()
                .withAgentOptions({keepAlive: true});
            httpAgent = httpClient.config.httpOptions.agent;

            return httpClient.send();
        });

        afterEach(() => {
            httpAgent.destroy();
        });

        it('rejects if socket doesn\'t connect', () => {
            const promise = httpClient
                .withConnectTimeout(50)
                .withBaseUrl('http://example.com:8989')
                .withAgentOptions({keepAlive: true})
                .send();

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('ConnectTimeoutError');
                    expect(err instanceof ConnectTimeoutError).toBeTruthy();
                });
        });

        it('does not close the socket when socket connects', () => {
            httpSpy.getHandler.and.callFake(() => {
                const returnValue = {statusCode: 200};

                return Promise.resolve()
                    .then(() => sleep(100))
                    .then(() => returnValue);
            });

            return httpClient.withConnectTimeout(50).send();
        });

        it('does not close the socket after socket has been reused', () => {
            httpSpy.getHandler.and.returnValue({
                statusCode: 200
            });

            const socketToBeReused = getOnlyFreeSocket();

            return Promise.resolve()
                .then(() => httpClient.withConnectTimeout(50).send())
                .then(() => expect(getOnlyFreeSocket()).toBe(socketToBeReused))
                .then(() => sleep(100))
                .then(() => expect(getOnlyFreeSocket()).toBe(socketToBeReused));
        });
    });
});
