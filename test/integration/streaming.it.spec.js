'use strict';

const concatStream = require('concat-stream');
const Readable = require('stream').Readable;

const describeIntegrationSupport = require('../support/describeIntegrationSupport');

const STREAMED_BODY_CONTENT = '{"c":"some text body which will be streamed"}';

describeIntegrationSupport('streaming', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;
    });

    it('should stream a response from a GET request', () => {
        httpSpy.getHandler.and.returnValue({
            body: STREAMED_BODY_CONTENT,
            statusCode: 200
        });

        let streamedBody;

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withResponseHandler((response) => {
                return response.pipe(concatStream((buffer) => {
                    streamedBody = buffer.toString();
                }));
            })
            .send()
            .then((result) => {
                expect(result.body).toBeUndefined();
                expect(streamedBody).toBe(STREAMED_BODY_CONTENT);
            });
    });

    it('should make a POST request with a streaming body', () => {
        httpSpy.postHandler.and.returnValue({
            statusCode: 200
        });

        const textStream = new Readable();
        textStream._read = () => {};
        textStream.push(STREAMED_BODY_CONTENT);
        textStream.push(null);

        return httpClient
            .withPath('/post')
            .withMethodPost()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .withHeaders({'Content-Type': 'application/json'})
            .withStreamBody(textStream)
            .send()
            .then(() => {
                expect(httpSpy.postHandler).toHaveBeenCalled();
                const args = httpSpy.postHandler.calls.argsFor(0)[0];
                expect(args.body).toEqual(JSON.parse(STREAMED_BODY_CONTENT));
            });
    });

});
