'use strict';

const describeIntegrationSupport = require('../support/describeIntegrationSupport');
const {UnexpectedHttpResponseCodeError} = require('../../lib/index').errors;

describeIntegrationSupport('expectStatus', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;

        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });
    });

    it('should accept expected response statuses', () => {
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .send();
    });

    it('should reject unexpected response statuses', () => {
        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([201])
            .withBufferJsonResponseHandler()
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err).toEqual(jasmine.any(UnexpectedHttpResponseCodeError));
                expect(err.response.statusCode).toBe(200);
                expect(err.response.body).toEqual({some: 'body'});
                expect(err.message).toBe('Request (method=GET;protocol=http:;hostname=localhost;port=8082;path=/get) returned unexpected HTTP response code: 200'); // eslint-disable-line max-len
            });
    });

});
