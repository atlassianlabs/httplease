'use strict';

const {ConnectTimeoutError} = require('../../lib/errors');

const describeIntegrationSupport = require('../support/describeIntegrationSupport');

const sleep = require('../support/sleep');

describeIntegrationSupport('makeRequest', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;
    });

    it('should make a GET request with the right parameters', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withParams({param: 'param value, with "special chars&foo=bar!"'})
            .withHeaders({custom: 'header with "special chars!"'})
            .send()
            .then((result) => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                const args = httpSpy.getHandler.calls.argsFor(0)[0];
                expect(args.query).toEqual({param: 'param value, with "special chars&foo=bar!"'});
                expect(args.headers.custom).toBe('header with "special chars!"');
                expect(result.statusCode).toBe(200);
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should make a POST request', () => {
        httpSpy.postHandler.and.returnValue({
            statusCode: 200
        });
        return httpClient
            .withPath('/post')
            .withMethodPost()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .send()
            .then(() => {
                expect(httpSpy.postHandler).toHaveBeenCalled();
            });
    });

    it('handles HTTP errors', () => {
        httpSpy.getHandler.and.returnValue({
            statusCode: 500
        });
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withResponseValidator(() => true)
            .withDiscardBodyResponseHandler()
            .send()
            .then((result) => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                expect(result.statusCode).toBe(500);
            });
    });

    it('handles socket errors by rejecting', () => {
        const promise = httpClient
            .withMethodGet()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .send({baseUrl: 'http://bad.invalid', path: '/foo'});

        return expectToReject(promise)
            .then((err) => {
                expect(httpSpy.getHandler).not.toHaveBeenCalled();
                expect(err.cause.code).toBe('ENOTFOUND');
            });
    });

    it('does not follow HTTP redirects', () => {
        httpSpy.getHandler.and.returnValue({
            statusCode: 301,
            headers: {
                'Location': 'http://invalid'
            }
        });
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withResponseValidator(() => true)
            .withDiscardBodyResponseHandler()
            .send()
            .then((result) => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                expect(result.statusCode).toBe(301);
            });
    });

    it('can discard response bodies', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .send()
            .then((result) => {
                expect(result.body).toBeUndefined();
            });
    });

    it('returns the clientName on any HTTP error', () => {
        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([500])
            .withDiscardBodyResponseHandler()
            .withClientName('some-client-name')
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err.clientName).toBe('some-client-name');
            });
    });

    describe('connectTimeouts', () => {

        it('rejects if socket doesn\'t connect', () => {
            const promise = httpClient
                .withMethodGet()
                .withExpectStatus([200])
                .withDiscardBodyResponseHandler()
                .withConnectTimeout(100)
                .withBaseUrl('http://example.com:8989')
                .send();

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('ConnectTimeoutError');
                    expect(err instanceof ConnectTimeoutError).toBeTruthy();
                });
        });

        it('does not close the socket when socket connects', () => {
            httpSpy.getHandler.and.callFake(() => {
                const returnValue = {statusCode: 200};

                return Promise.resolve()
                    .then(() => sleep(100))
                    .then(() => returnValue);
            });

            return httpClient
                .withMethodGet()
                .withPath('/get')
                .withExpectStatus([200])
                .withDiscardBodyResponseHandler()
                .withConnectTimeout(50)
                .send();
        });
    });

});
