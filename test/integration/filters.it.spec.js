'use strict';

const describeIntegrationSupport = require('../support/describeIntegrationSupport');

describeIntegrationSupport('filters', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;
    });

    it('allows filters to modify the requestConfig and replace the response', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });
        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withFilter((requestConfig, next) => {
                requestConfig.httpOptions.headers.custom = 'some-custom-header';
                return next(requestConfig).then(() => 'some-custom-response');
            })
            .send()
            .then((result) => {
                expect(httpSpy.getHandler).toHaveBeenCalled();
                const args = httpSpy.getHandler.calls.argsFor(0)[0];
                expect(args.headers.custom).toBe('some-custom-header');
                expect(result).toBe('some-custom-response');
            });
    });

    it('allows filters to throw an error', () => {
        const expectedException = new Error('some custom error');
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });
        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withFilter(() => {
                throw expectedException;
            })
            .send();
        return expectToReject(promise)
            .then((error) => {
                expect(error).toBe(expectedException);
            });
    });

});
