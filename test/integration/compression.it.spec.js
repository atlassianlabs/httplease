'use strict';
/* eslint no-sync: "off" */

const concatStream = require('concat-stream');
const describeIntegrationSupport = require('../support/describeIntegrationSupport');
const zlib = require('zlib');

describeIntegrationSupport('compression', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;
    });

    it('should parse a compressed JSON response body', () => {
        httpSpy.getHandler.and.returnValue({
            body: zlib.deflateSync(JSON.stringify({some: 'body'})),
            headers: {
                'content-encoding': 'gzip',
                'content-type': 'application/json'
            },
            statusCode: 200
        });

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .send()
            .then((response) => {
                expect(response.body).toEqual({some: 'body'});
            });
    });

    it('should stream a compressed response', () => {
        httpSpy.getHandler.and.returnValue({
            body: zlib.deflateSync(`Some compressed text ${'!'.repeat(1024*1024)}`),
            headers: {'content-encoding': 'gzip'},
            statusCode: 200
        });

        let streamedBody;

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withResponseHandler((response) => {
                return response.pipe(concatStream((buffer) => {
                    streamedBody = buffer.toString();
                }), {decompress: true});
            })
            .send()
            .then(() => {
                expect(streamedBody).toMatch(/^Some compressed text/);
                expect(streamedBody.length).toBeGreaterThan(1024*1024);
            });
    });

});
