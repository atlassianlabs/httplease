'use strict';

const describeIntegrationSupport = require('../support/describeIntegrationSupport');
const {UnexpectedHttpResponseCodeError} = require('../../lib/index').errors;

describeIntegrationSupport('buffering', (support) => {

    let httpSpy;
    let httpClient;

    beforeEach(() => {
        httpSpy = support.httpSpy;
        httpClient = support.httpClient;
    });

    it('should receive a JSON response body', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .send()
            .then((result) => {
                expect(result.body).toEqual({some: 'body'});
            });
    });

    it('should receive a Buffer response body', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });

        return httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferBodyResponseHandler()
            .send()
            .then((result) => {
                expect(result.body instanceof Buffer).toBe(true);
                expect(result.body.toString()).toBe('{"some":"body"}');
            });
    });

    it('should send a JSON request body', () => {
        httpSpy.postHandler.and.returnValue({
            statusCode: 200
        });

        return httpClient
            .withPath('/post')
            .withMethodPost()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .withJsonBody({someValue: 'some value'})
            .send()
            .then(() => {
                const args = httpSpy.postHandler.calls.argsFor(0)[0];
                expect(args.body).toEqual({someValue: 'some value'});
            });
    });

    it('should send and receive JSON with unicode strings', () => {
        httpSpy.postHandler.and.returnValue({
            body: {text: 'response 日本語'},
            statusCode: 200
        });

        return httpClient
            .withPath('/post')
            .withMethodPost()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withJsonBody({text: 'request 日本語'})
            .send()
            .then((response) => {
                expect(response.body).toEqual({text: 'response 日本語'});

                const args = httpSpy.postHandler.calls.argsFor(0)[0];
                expect(args.body).toEqual({text: 'request 日本語'});
            });
    });

    it('should send a buffer request body', () => {
        httpSpy.postHandler.and.returnValue({
            statusCode: 200
        });
        return httpClient
            .withPath('/post')
            .withMethodPost()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .withHeaders({'content-type': 'application/json'})
            .withBufferBody(Buffer.from('{"someValue": "some value"}', 'utf-8'))
            .send()
            .then(() => {
                const args = httpSpy.postHandler.calls.argsFor(0)[0];
                expect(args.body).toEqual({someValue: 'some value'});
            });
    });

    it('handles oversized response bodies by rejecting', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 200
        });

        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferJsonResponseHandler(1)
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err.message).toBe('Overflow response body');
            });
    });

    it('rejects with error when a 200 response body cannot be parsed as JSON', () => {
        httpSpy.getHandler.and.returnValue({
            body: 'some string body',
            statusCode: 200
        });

        const promise = httpClient
            .withPath('/get')
            .withExpectStatus([200])
            .withBufferJsonResponseHandler()
            .withMethodGet()
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err.message).toBe('Response was not JSON! Content-Type: text/html; charset=utf-8');
            });
    });

    it('returns JSON body for an error response which is JSON', () => {
        httpSpy.getHandler.and.returnValue({
            body: {some: 'body'},
            statusCode: 500
        });

        const promise = httpClient
            .withPath('/get')
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .withMethodGet()
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err).toEqual(jasmine.any(UnexpectedHttpResponseCodeError));
                expect(err.response.statusCode).toBe(500);
                expect(err.response.body).toEqual({some: 'body'});
            });
    });

    it('returns Buffer body for an error response which is not JSON', () => {
        httpSpy.getHandler.and.returnValue({
            body: 'some string body',
            statusCode: 500
        });

        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err).toEqual(jasmine.any(UnexpectedHttpResponseCodeError));
                expect(err.response.statusCode).toBe(500);
                expect(err.response.getHeader('Content-Type')).toBe('text/html; charset=utf-8');
                expect(err.response.body).toEqual(Buffer.from('some string body'));
            });
    });

    it('pretty prints errors with buffer response bodies', () => {
        httpSpy.getHandler.and.returnValue({
            body: 'some string body',
            statusCode: 500
        });

        const promise = httpClient
            .withPath('/get')
            .withMethodGet()
            .withExpectStatus([200])
            .withDiscardBodyResponseHandler()
            .send();

        return expectToReject(promise)
            .then((err) => {
                const json = JSON.stringify(err);
                expect(json).toContain('"body":"some string body"');
            });
    });

});
