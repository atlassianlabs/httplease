'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();

describe('pipeline', () => {
    let requestConfig;
    let mockRequest;
    let fakeHttpResponse;
    let executePipeline;

    beforeEach(() => {
        requestConfig = {
            filters: [],
            headers: {},
            params: {},
            httpOptions: {
                method: 'FAKE'
            }
        };

        fakeHttpResponse = {
            some: 'fake response'
        };

        mockRequest = jasmine.createSpy('makeRequest');
        mockRequest.and.returnValue(Promise.resolve(fakeHttpResponse));

        executePipeline = requireWithMocks('../../lib/pipeline', {
            './makeRequest': {makeRequest: mockRequest}
        }).executePipeline;
    });

    it('should resolve with makeRequest response', () => {
        return executePipeline(requestConfig)
            .then((response) => {
                expect(mockRequest).toHaveBeenCalled();
                expect(response).toBe(fakeHttpResponse);
            });
    });

    it('should reject if makeRequest rejects', () => {
        const expectedError = new Error('fake error');
        mockRequest.and.returnValue(Promise.reject(expectedError));

        return expectToReject(executePipeline(requestConfig))
            .then((error) => {
                expect(mockRequest).toHaveBeenCalled();
                expect(error).toBe(expectedError);
            });
    });

    it('should pass requestConfig through', () => {
        requestConfig.headers = {
            someHeader: 'some value'
        };
        requestConfig.params = {
            someParam: 'some value'
        };
        requestConfig.extraData = 'some data';

        return executePipeline(requestConfig)
            .then(() => {
                expect(mockRequest).toHaveBeenCalledWith({
                    headers: {
                        someHeader: 'some value'
                    },
                    params: {
                        someParam: 'some value'
                    },
                    httpOptions: {
                        method: 'FAKE'
                    },
                    extraData: 'some data'
                });
            });
    });

    it('should copy clientName onto errors', () => {
        const expectedError = new Error('fake error');
        mockRequest.and.returnValue(Promise.reject(expectedError));

        requestConfig.clientName = 'some-name';
        return expectToReject(executePipeline(requestConfig))
            .then((err) => {
                expect(err).toBe(expectedError);
                expect(err.clientName).toBe('some-name');
            });
    });

    it('should not set an undefined clientName on errors', () => {
        const expectedError = new Error('fake error');
        mockRequest.and.returnValue(Promise.reject(expectedError));

        return expectToReject(executePipeline(requestConfig))
            .then((err) => {
                expect(err).toBe(expectedError);
                expect(Object.keys(err)).not.toContain('clientName');
            });
    });

    describe('filters', () => {
        function identityFilter(requestConfig, next) {
            return next(requestConfig);
        }

        let firstFilter;
        let secondFilter;

        beforeEach(() => {
            firstFilter = jasmine.createSpy('firstFilter');
            firstFilter.and.callFake(identityFilter);

            secondFilter = jasmine.createSpy('secondFilter');
            secondFilter.and.callFake(identityFilter);

            requestConfig.filters = [firstFilter, secondFilter];
        });

        it('handles filters being undefined', () => {
            requestConfig.filters = undefined;

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(response).toBe(fakeHttpResponse);
                });
        });

        it('can modify the requestConfig', () => {
            firstFilter.and.callFake((requestConfig, next) => {
                requestConfig.headers = {test: 'testHeader'};
                return next(requestConfig);
            });
            secondFilter.and.callFake((requestConfig, next) => {
                requestConfig.headers['test2'] = 'testHeader2';
                return next(requestConfig);
            });

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(firstFilter).toHaveBeenCalled();
                    expect(secondFilter).toHaveBeenCalled();
                    expect(mockRequest.calls.argsFor(0)[0].headers).toEqual({
                        test: 'testHeader',
                        test2: 'testHeader2'
                    });
                    expect(response).toEqual(fakeHttpResponse);
                });
        });

        it('can replace the response and skip the HTTP request', () => {
            const expectedResult = {custom: 'result'};

            firstFilter.and.callFake(() => {
                return Promise.resolve(expectedResult);
            });

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(mockRequest).not.toHaveBeenCalled();
                    expect(firstFilter).toHaveBeenCalled();
                    expect(secondFilter).not.toHaveBeenCalled();
                    expect(response).toBe(expectedResult);
                });
        });

        it('can modify the response after the HTTP request has been made', () => {
            firstFilter.and.callFake((requestConfig, next) => {
                return next(requestConfig)
                    .then((result) => {
                        expect(result.extraData).toBe('enriched extra stuff');
                        result.extraData = 'even more enriched extra stuff!';
                        return result;
                    });
            });

            secondFilter.and.callFake((requestConfig, next) => {
                return next(requestConfig)
                    .then((result) => {
                        result.extraData = 'enriched extra stuff';
                        return result;
                    });
            });

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(mockRequest).toHaveBeenCalled();
                    expect(response.extraData).toBe('even more enriched extra stuff!');
                });
        });

        it('should reject if filter rejects before HTTP request', () => {
            const expectedError = new Error('fake error');
            firstFilter.and.returnValue(Promise.reject(expectedError));

            return expectToReject(executePipeline(requestConfig))
                .then((error) => {
                    expect(firstFilter).toHaveBeenCalled();
                    expect(secondFilter).not.toHaveBeenCalled();
                    expect(mockRequest).not.toHaveBeenCalled();
                    expect(error).toEqual(expectedError);
                });
        });

        it('should reject with error if filter throws an error before HTTP request', () => {
            const expectedError = new Error('fake error');
            firstFilter.and.callFake(() => {
                throw expectedError;
            });

            return expectToReject(executePipeline(requestConfig))
                .then((error) => {
                    expect(firstFilter).toHaveBeenCalled();
                    expect(secondFilter).not.toHaveBeenCalled();
                    expect(mockRequest).not.toHaveBeenCalled();
                    expect(error).toEqual(expectedError);
                });
        });

        it('should reject if filter rejects after HTTP request', () => {
            const expectedError = new Error('fake error');
            firstFilter.and.callFake((requestConfig, next) => {
                return next(requestConfig)
                    .then(() => { throw expectedError; });
            });

            return expectToReject(executePipeline(requestConfig))
                .then((error) => {
                    expect(firstFilter).toHaveBeenCalled();
                    expect(secondFilter).toHaveBeenCalled();
                    expect(mockRequest).toHaveBeenCalled();
                    expect(error).toEqual(expectedError);
                });
        });

        it('handles filter trying to return result of a second call to next()', () => {
            firstFilter.and.callFake((requestConfig, next) => {
                next(requestConfig); // nobody should ever do this
                return next(requestConfig);
            });

            const expectedResponse = {
                fake: 'fake response'
            };
            secondFilter.and.returnValue(Promise.resolve(expectedResponse));

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(secondFilter).toHaveBeenCalled();
                    expect(response).toBe(expectedResponse);
                });
        });

        it('handles filter calling next() more than once but returning the first result', () => {
            firstFilter.and.callFake((requestConfig, next) => {
                const p = next(requestConfig); // eslint-disable-line callback-return
                next(requestConfig); // nobody should ever do this!
                return p;
            });

            const expectedResponse = {
                fake: 'fake response'
            };
            secondFilter.and.returnValue(Promise.resolve(expectedResponse));

            return executePipeline(requestConfig)
                .then((response) => {
                    expect(secondFilter).toHaveBeenCalled();
                    expect(response).toBe(expectedResponse);
                });
        });

    }); // end 'filters'

    describe('filter ordering', () => {

        let tracking;

        beforeEach(() => {
            tracking = [];
        });

        function registerDummyFilter(name, order) {
            const filter = jasmine.createSpy(`${name}Filter`);
            filter.and.callFake((requestConfig, next) => {
                tracking.push(`${name} request`);
                return next(requestConfig)
                    .then(() => tracking.push(`${name} response`));
            });
            if (order) {
                filter.order = order;
            }
            requestConfig.filters.push(filter);
        }

        it('runs the filters in the array order', () => {
            registerDummyFilter('first');
            registerDummyFilter('second');

            return executePipeline(requestConfig)
                .then(() => {
                    expect(tracking).toEqual([
                        'first request',
                        'second request',
                        'second response',
                        'first response'
                    ]);
                });
        });

        it('runs the filters in the order specified by the function property', () => {
            registerDummyFilter('second', 20);
            registerDummyFilter('first', 10);

            return executePipeline(requestConfig)
                .then(() => {
                    expect(tracking).toEqual([
                        'first request',
                        'second request',
                        'second response',
                        'first response'
                    ]);
                });
        });

        it('maintains a stable ordering for large arrays', () => {
            const expectedTracking1 = [];
            const expectedTracking2 = [];
            for (let i = 0; i < 500; ++i) {
                registerDummyFilter(`filter${i}`);
                expectedTracking1.push(`filter${i} request`);
                expectedTracking2.push(`filter${i} response`);
            }
            const expectedTracking = expectedTracking1.concat(expectedTracking2.reverse());

            return executePipeline(requestConfig)
                .then(() => {
                    expect(tracking).toEqual(expectedTracking);
                });
        });

        it('inserts filters with order=0 by default', () => {
            registerDummyFilter('late', 1);
            registerDummyFilter('default1');
            registerDummyFilter('default2');
            registerDummyFilter('early', -1);

            return executePipeline(requestConfig)
                .then(() => {
                    expect(tracking).toEqual([
                        'early request',
                        'default1 request',
                        'default2 request',
                        'late request',
                        'late response',
                        'default2 response',
                        'default1 response',
                        'early response'
                    ]);
                });
        });


    }); // end 'filter ordering'

});
