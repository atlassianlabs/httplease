'use strict';

const WrappedResponse = require('../../lib/wrappedResponse').WrappedResponse;
const responseHandlerHelpers = require('../../lib/responseHandler');

/* eslint no-loop-func: "off" */

describe('responseHandler', () => {

    let expectedStatusCode;
    let fakeResponse;

    beforeEach(() => {
        expectedStatusCode = 4242;
        const httpResponse = {
            statusCode: expectedStatusCode,
            headers: {some: 'header'},
            pause: () => {}
        };

        fakeResponse = new WrappedResponse(httpResponse);

        fakeResponse.bufferJson = jasmine.createSpy('bufferJson')
            .and.returnValue(Promise.resolve({some: 'body'}));

        fakeResponse.bufferBody = jasmine.createSpy('bufferBody')
            .and.returnValue(Promise.resolve(Buffer.from('the body')));

        fakeResponse.discard = jasmine.createSpy('discard')
            .and.returnValue(Promise.resolve());
    });

    function withContentType(contentType) {
        fakeResponse.headers['content-type'] = contentType;
    }

    function withJsonFromBufferBody() {
        const json = JSON.stringify({some: 'body'});
        fakeResponse.bufferBody.and.returnValue(Promise.resolve(Buffer.from(json)));
    }

    function expectStandardFormat(response, body) {
        expect(response).toEqual({
            statusCode: expectedStatusCode,
            headers: fakeResponse.headers,
            body,
            getHeader: jasmine.any(Function)
        });
        expect(response.getHeader('some')).toBe('header');
    }

    function expectJson(response) {
        const body = {some: 'body'};
        expectStandardFormat(response, body);
        expect(fakeResponse.bufferJson).toHaveBeenCalled();
        expect(fakeResponse.bufferBody).not.toHaveBeenCalled();
        expect(fakeResponse.discard).not.toHaveBeenCalled();
    }

    function expectJsonFromBuffer(response) {
        const body = {some: 'body'};
        expectStandardFormat(response, body);
        expect(fakeResponse.bufferJson).not.toHaveBeenCalled();
        expect(fakeResponse.bufferBody).toHaveBeenCalled();
        expect(fakeResponse.discard).not.toHaveBeenCalled();
    }

    function expectBuffer(response) {
        const body = Buffer.from('the body');
        expectStandardFormat(response, body);
        expect(fakeResponse.bufferJson).not.toHaveBeenCalled();
        expect(fakeResponse.bufferBody).toHaveBeenCalled();
        expect(fakeResponse.discard).not.toHaveBeenCalled();
    }

    function expectDiscard(response) {
        const body = undefined;
        expectStandardFormat(response, body);
        expect(fakeResponse.bufferJson).not.toHaveBeenCalled();
        expect(fakeResponse.bufferBody).not.toHaveBeenCalled();
        expect(fakeResponse.discard).toHaveBeenCalled();
    }

    const jsonTests = [
        {
            contentType: 'application/json',
            description: 'normal Content-Type',
            isJson: true
        },
        {
            contentType: 'application/foo+json',
            description: 'Content-Type with plus prefix',
            isJson: true
        },
        {
            contentType: 'application/json; charset=utf-8',
            description: 'Content-Type with charset',
            isJson: true
        },
        {
            contentType: ' APPLICATION/JSON ; CHARSET=UTF-8 ',
            description: 'Content-Type in uppercase with spaces',
            isJson: true
        },
        {
            contentType: 'text/json',
            description: 'text/json Content-Type',
            isJson: true
        },
        {
            contentType: 'application/json; something=value',
            description: 'Content-Type with ignored parameter',
            isJson: true
        },
        {
            contentType: 'application/json; something=value; charset=utf-8',
            description: 'Content-Type with utf-8 and ignored parameter',
            isJson: true
        },
        {
            contentType: 'application/json; ignored',
            description: 'Content-Type with parameter missing a value is ignored',
            isJson: true
        },
        {
            contentType: 'application/json charset=utf-8',
            description: 'Content-Type missing semi-colon',
            isJson: false
        },
        {
            contentType: 'application/json; charset=ascii',
            description: 'invalid charset in Content-Type',
            isJson: false
        },
        {
            contentType: 'not-json',
            description: 'Content-Type not JSON',
            isJson: false
        },
        {
            contentType: 'application/json/with/lots/of/slashes',
            description: 'Content-Type not JSON with lots of slashes',
            isJson: false
        },
        {
            contentType: 'application/json+foo',
            description: 'Content-Type with plus suffix',
            isJson: false
        },
        {
            contentType: undefined,
            description: 'missing Content-Type',
            isJson: false
        }
    ];

    describe('bufferJsonResponseHandler', () => {

        function runResponseHandler() {
            return responseHandlerHelpers.bufferJsonResponseHandler(fakeResponse);
        }

        for (const test of jsonTests) {

            if (test.isJson) {
                it(`parses JSON to standard format with ${test.description}`, () => {
                    withContentType(test.contentType);

                    return runResponseHandler().then(expectJson);
                });
            } else {
                it(`throws error when given ${test.description}`, () => {
                    withContentType(test.contentType);

                    return expectToReject(runResponseHandler())
                        .then((err) => {
                            expect(fakeResponse.discard).toHaveBeenCalledWith();
                            expect(err.constructor.name).toBe('NotJsonContentTypeError');
                            expect(err.name).toBe('NotJsonContentTypeError');
                            expect(err.message).toBe(`Response was not JSON! Content-Type: ${test.contentType}`);
                        });
                });
            }
        }

        it('does not parse JSON when the response.statusCode==204', () => {
            fakeResponse.statusCode = expectedStatusCode = 204;

            return runResponseHandler().then(expectDiscard);
        });

        it('does not parse JSON when Content-Length == 0', () => {
            fakeResponse.headers['content-length'] = '0';

            return runResponseHandler().then(expectDiscard);
        });

        it('passes opts through to WrappedResponse', () => {
            withContentType('application/json');
            const opts = {foo: 42};

            return responseHandlerHelpers.bufferJsonResponseHandler(fakeResponse, opts)
                .then(() => {
                    expect(fakeResponse.bufferJson).toHaveBeenCalledWith(opts);
                });
        });

    }); // end bufferJsonResponseHandler


    describe('defaultErrorResponseHandler', () => {

        function runResponseHandler() {
            return responseHandlerHelpers.defaultErrorResponseHandler(fakeResponse);
        }

        for (const test of jsonTests) {

            if (test.isJson) {
                it(`parses JSON from buffer to standard format with ${test.description}`, () => {
                    withContentType(test.contentType);
                    withJsonFromBufferBody();

                    return runResponseHandler().then(expectJsonFromBuffer);
                });

                it(`returns buffered body when JSON parsing fails with ${test.description}`, () => {
                    withContentType(test.contentType);

                    return runResponseHandler().then(expectBuffer);
                });
            } else {
                it(`returns buffered body when given ${test.description}`, () => {
                    withContentType(test.contentType);

                    return runResponseHandler().then(expectBuffer);
                });
            }
        }

    }); // end defaultErrorResponseHandler


    describe('bufferBodyResponseHandler', () => {

        it('has standard format', () => {
            return responseHandlerHelpers.bufferBodyResponseHandler(fakeResponse)
                .then(expectBuffer);
        });

        it('passes opts through to WrappedResponse', () => {
            const opts = {foo: 42};

            return responseHandlerHelpers.bufferBodyResponseHandler(fakeResponse, opts)
                .then(() => {
                    expect(fakeResponse.bufferBody).toHaveBeenCalledWith(opts);
                });
        });

    }); // end bufferBodyResponseHandler


    describe('discardBodyResponseHandler', () => {

        it('has standard format', () => {
            return responseHandlerHelpers.discardBodyResponseHandler(fakeResponse)
                .then(expectDiscard);
        });

    }); // end discardBodyResponseHandler

});
