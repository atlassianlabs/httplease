'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();

const events = require('events');
const {defaultConfig} = require('../../lib/defaultConfig');
const responseHandlerHelpers = require('../../lib/responseHandler');
const {mergeConfig} = require('../../lib/utils/mergeConfig');

describe('makeRequest', () => {
    let mockHttpRequest;
    let mockHttpResponse;
    let mockHttp;
    let mockHttps;

    let makeRequest;

    let requestConfig;

    beforeEach(() => {
        mockHttpRequest = new events.EventEmitter();
        mockHttpRequest.end = jasmine.createSpy('mockRequestEnd');
        mockHttpRequest.abort = jasmine.createSpy('mockRequestAbort');
        mockHttpRequest.setTimeout = jasmine.createSpy('mockSetTimeout');
        mockHttpResponse = new events.EventEmitter();
        mockHttpResponse.pause = jasmine.createSpy('mockRequestPause');
        mockHttpResponse.resume = jasmine.createSpy('mockRequestResume');
        mockHttpResponse.statusCode = 200;
        mockHttpResponse.headers = {
            'content-type': 'application/json'
        };

        mockHttp = jasmine.createSpyObj('mockHttp', ['request']);
        mockHttp.request.and.returnValue(mockHttpRequest);
        mockHttp['@global'] = true;
        mockHttps = jasmine.createSpyObj('mockHttps', ['request']);
        mockHttps.request.and.returnValue(mockHttpRequest);
        mockHttps['@global'] = true;

        ({makeRequest} = requireWithMocks('../../lib/makeRequest', {
            'http': mockHttp,
            'https': mockHttps
        }));

        requestConfig = mergeConfig(defaultConfig, {
            responseValidator: () => true,
            responseHandler: responseHandlerHelpers.bufferJsonResponseHandler,
            timeout: 1000,
            httpOptions: {
                protocol: 'http:',
                hostname: 'example.com',
                path: '/thePath',
                method: 'GET'
            }
        });
    });


    function makeSuccessfulRequest() {
        setImmediate(() => {
            mockHttpRequest.emit('response', mockHttpResponse);
            setImmediate(() => {
                mockHttpResponse.emit('data', Buffer.from('{"some": "json"}'));
                mockHttpResponse.emit('end', 'ignored value');
            });
        });
        return makeRequest(requestConfig);
    }

    function makeRequestFailImmediately(failFn) {
        setImmediate(() => {
            failFn();
        });
        return makeRequest(requestConfig);
    }

    function makeRequestFailAfterResponseStarts(failFn) {
        setImmediate(() => {
            mockHttpRequest.emit('response', mockHttpResponse);
            setImmediate(() => {
                failFn();
            });
        });
        return makeRequest(requestConfig);
    }

    function expectWrappedResponseObject(wrappedResponse) {
        expect(wrappedResponse._httpResponse).toBe(mockHttpResponse);
        expect(typeof wrappedResponse.bufferBody).toBe('function');
    }

    describe('basic request', () => {

        it('passes the correct options to http.request()', () => {
            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttp.request).toHaveBeenCalledWith({
                        protocol: 'http:',
                        method: 'GET',
                        hostname: 'example.com',
                        path: '/thePath'
                    });
                    expect(mockHttps.request).not.toHaveBeenCalled();
                });
        });

        it('passes the correct options to https.request()', () => {
            requestConfig.httpOptions.protocol = 'https:';

            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttps.request).toHaveBeenCalledWith({
                        protocol: 'https:',
                        method: 'GET',
                        hostname: 'example.com',
                        path: '/thePath'
                    });
                    expect(mockHttp.request).not.toHaveBeenCalled();
                });
        });

        it('fails with unknown protocol', () => {
            requestConfig.httpOptions.protocol = 'foo:';

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause.message).toBe("No client for protocol 'foo:'");
                });
        });

        it('supports custom ports', () => {
            requestConfig.httpOptions.port = 42;

            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttp.request).toHaveBeenCalledWith(jasmine.objectContaining({
                        hostname: 'example.com',
                        port: 42,
                        path: '/thePath'
                    }));
                });
        });

        it('fails if missing protocol', () => {
            delete requestConfig.httpOptions.protocol;

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.message).toMatch(/Missing option 'protocol'/);
                });
        });

        it('fails if missing hostname', () => {
            delete requestConfig.httpOptions.hostname;

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.message).toMatch(/Missing option 'hostname'/);
                });
        });

        it('fails if missing path', () => {
            delete requestConfig.httpOptions.path;

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.message).toMatch(/Missing option 'path'/);
                });
        });

        it('passes the agent to https.request() without cloning', () => {
            const agent = {some: 'agent'};
            requestConfig.httpOptions.agent = agent;

            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttp.request.calls.argsFor(0)[0].agent).toBe(agent);
                });
        });

        it('fails if method is missing', () => {
            delete requestConfig.httpOptions.method;

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.name).toBe('InvalidHttpleaseConfigError');
                    expect(err.message).toMatch(/Missing option 'method'/);
                });
        });

    }); // end 'basic request'

    describe('timeout', () => {
        it('sets timeout on request', () => {
            requestConfig.timeout = 4242;

            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttpRequest.setTimeout).toHaveBeenCalledWith(4242);
                });
        });

        it('rejects promise if request times out', () => {
            requestConfig.timeout = 4242;

            const promise = makeRequestFailImmediately(() => mockHttpRequest.emit('timeout'));

            return expectToReject(promise)
                .then((err) => {
                    expect(mockHttpRequest.abort).toHaveBeenCalledWith();
                    expect(err.constructor.name).toEqual('TimeoutError');
                    expect(err.name).toEqual('TimeoutError');
                    expect(err.message).toMatch(/.*has timed out after 4242msec$/);
                });
        });

        describe('clears connect timeout', () => {
            it('when socket was already connected', () => {
                const clearTimeoutSpy = spyOn(global, 'clearTimeout').and.callThrough();
                const promise = makeSuccessfulRequest();

                const socket = new events.EventEmitter();
                socket.connecting = false;

                mockHttpRequest.emit('socket', socket);
                expect(clearTimeoutSpy).toHaveBeenCalledTimes(1);

                return promise;
            });

            it('when socket gets connected after it was assigned to the request', () => {
                const clearTimeoutSpy = spyOn(global, 'clearTimeout').and.callThrough();
                const promise = makeSuccessfulRequest();

                const socket = new events.EventEmitter();
                socket.connecting = true;

                mockHttpRequest.emit('socket', socket);
                expect(clearTimeoutSpy).not.toHaveBeenCalled();
                socket.emit('connect');
                expect(clearTimeoutSpy).toHaveBeenCalledTimes(1);

                return promise;
            });
        }); // end 'clears connect timeout on socket connect event'

        describe('times out if socket does not connect quickly enough', () => {
            beforeEach(() => {
                jasmine.clock().install();
            });

            afterEach(() => {
                jasmine.clock().uninstall();
            });

            function makeRequestAndClockTick(millis) {
                const promise = makeRequest(requestConfig);

                const socket = new events.EventEmitter();
                socket.connecting = true;
                mockHttpRequest.emit('socket', socket);

                jasmine.clock().tick(millis);

                return promise;
            }

            it('when connectTimeout is set', () => {
                requestConfig.connectTimeout = 2000;
                requestConfig.timeout = 5000;

                const promise = makeRequestAndClockTick(requestConfig.connectTimeout);

                return expectToReject(promise)
                    .then((err) => {
                        expect(mockHttpRequest.abort).toHaveBeenCalledWith();
                        expect(err.constructor.name).toEqual('ConnectTimeoutError');
                        expect(err.name).toEqual('ConnectTimeoutError');
                        expect(err.message)
                            .toMatch(/.*has timed out after 2000msec while trying to connect$/);
                    });
            });

            it('when connectTimeout is not set', () => {
                requestConfig.timeout = 4242;

                const promise = makeRequestAndClockTick(requestConfig.timeout);

                return expectToReject(promise)
                    .then((err) => {
                        expect(mockHttpRequest.abort).toHaveBeenCalledWith();
                        expect(err.constructor.name).toEqual('ConnectTimeoutError');
                        expect(err.name).toEqual('ConnectTimeoutError');
                        expect(err.message)
                            .toMatch(/.*has timed out after 4242msec while trying to connect$/);
                    });
            });

            it('rejects with a error that is an instance of TimeoutError', () => {
                requestConfig.connectTimeout = 2000;

                const promise = makeRequestAndClockTick(requestConfig.connectTimeout);

                return expectToReject(promise)
                    .then((err) => {
                        expect(err.constructor.name).toEqual('ConnectTimeoutError');
                        expect(err.name).toEqual('ConnectTimeoutError');
                    });
            });

        }); // end 'times out if socket does not connect quickly enough'

    }); // end timeout

    describe('requests with a body', () => {

        let expectedError;

        beforeEach(() => {
            expectedError = new Error('fake error');
        });

        it('calls request.end() by default', () => {
            return makeSuccessfulRequest()
                .then(() => {
                    expect(mockHttpRequest.end).toHaveBeenCalledWith();
                });
        });

        it('rejects the promise if request errors immediately', () => {
            const promise = makeRequestFailImmediately(() => mockHttpRequest.emit('error', expectedError));

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

        it('rejects the promise if request errors after response starts', () => {
            const promise = makeRequestFailAfterResponseStarts(() => mockHttpRequest.emit('error', expectedError));

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

    }); // end requests with a body

    describe('responseHandler', () => {

        let expectedError;

        beforeEach(() => {
            expectedError = new Error('fake error');
        });

        it('calls responseValidator with the WrappedResponse object', () => {
            requestConfig.responseValidator = jasmine.createSpy('mockResponseValidator').and.returnValue(true);

            return makeSuccessfulRequest()
                .then(() => {
                    expect(requestConfig.responseValidator).toHaveBeenCalled();
                    expectWrappedResponseObject(requestConfig.responseValidator.calls.argsFor(0)[0]);
                });
        });

        it('calls responseHandler with the WrappedResponse object', () => {
            requestConfig.responseHandler = jasmine.createSpy('mockResponseHandler').and.returnValue(Promise.resolve());

            return makeSuccessfulRequest()
                .then(() => {
                    expect(requestConfig.responseHandler).toHaveBeenCalled();
                    expectWrappedResponseObject(requestConfig.responseHandler.calls.argsFor(0)[0]);
                });
        });

        it('calls responseHandler synchronously after receiving the response', () => new Promise((done) => {
            requestConfig.responseHandler = jasmine.createSpy('mockResponseHandler').and.returnValue(Promise.resolve());
            makeRequest(requestConfig)
                .catch(() => {}); // ignore errors

            setImmediate(() => {
                mockHttpRequest.emit('response', mockHttpResponse);
                setImmediate(() => {
                    mockHttpResponse.emit('end');
                    expect(requestConfig.responseHandler).toHaveBeenCalled();
                    done();
                });
            });
        }));

        it('does not call errorResponseHandler', () => {
            requestConfig.errorResponseHandler = jasmine.createSpy('mockErrorResponseHandler');

            return makeSuccessfulRequest()
                .then(() => {
                    expect(requestConfig.errorResponseHandler).not.toHaveBeenCalled();
                });
        });

        it('resolves with the result of responseHandler', () => {
            const expectedResult = {some: 'object'};
            requestConfig.responseHandler = () => Promise.resolve(expectedResult);

            return makeSuccessfulRequest()
                .then((result) => {
                    expect(result).toBe(expectedResult);
                });
        });

        it('does not resolve early from responseHandler, waits until response ends', () => new Promise((done, fail) => {
            requestConfig.responseHandler = () => Promise.resolve();
            const promise = makeRequest(requestConfig)
                .catch(() => {}); // ignore errors

            setImmediate(() => {
                mockHttpRequest.emit('response', mockHttpResponse);
                setImmediate(() => {
                    promise.then(() => fail(new Error('Expected promise to not be resolved yet')));
                    setImmediate(() => {
                        done();
                    });
                });
            });
        }));

        it('does not resolve early from response, waits for responseHandler', () => new Promise((done, fail) => {
            requestConfig.responseHandler = () => new Promise(() => {}); // never resolve!
            const promise = makeRequest(requestConfig)
                .catch(() => {}); // ignore errors

            setImmediate(() => {
                mockHttpRequest.emit('response', mockHttpResponse);
                setImmediate(() => {
                    mockHttpResponse.emit('end');
                    setImmediate(() => {
                        promise.then(() => fail(new Error('Expected promise to not be resolved yet')));
                        setImmediate(() => {
                            done();
                        });
                    });
                });
            });
        }));

        it('rejects if the response errors', () => {
            const promise = makeRequestFailAfterResponseStarts(() => mockHttpResponse.emit('error', expectedError));

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

        it('rejects if the responseHandler rejects', () => {
            requestConfig.responseHandler = () => Promise.reject(expectedError);

            const promise = makeSuccessfulRequest();

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

        it('rejects if the responseHandler throws', () => {
            requestConfig.responseHandler = () => {
                throw expectedError;
            };

            const promise = makeSuccessfulRequest();

            return expectToReject(promise)
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

    }); // end responseHandler

    describe('errorResponseHandler', () => {

        let expectedError;

        beforeEach(() => {
            requestConfig.responseValidator = () => false;

            expectedError = new Error('fake error');
        });

        it('calls errorResponseHandler with the WrappedResponse object', () => {
            requestConfig.errorResponseHandler = jasmine.createSpy('mockResponseHandler');
            requestConfig.errorResponseHandler.and.returnValue(Promise.resolve({}));

            return expectToReject(makeSuccessfulRequest())
                .then(() => {
                    expect(requestConfig.errorResponseHandler).toHaveBeenCalled();
                    expectWrappedResponseObject(requestConfig.errorResponseHandler.calls.argsFor(0)[0]);
                });
        });

        it('calls errorResponseHandler synchronously after receiving the response', () => new Promise((done) => {
            requestConfig.errorResponseHandler = jasmine.createSpy('mockResponseHandler');
            requestConfig.errorResponseHandler.and.returnValue(Promise.resolve({}));
            makeRequest(requestConfig)
                .catch(() => {}); // ignore errors

            setImmediate(() => {
                mockHttpRequest.emit('response', mockHttpResponse);
                setImmediate(() => {
                    mockHttpResponse.emit('end');
                    expect(requestConfig.errorResponseHandler).toHaveBeenCalled();
                    done();
                });
            });
        }));

        it('does not call responseHandler', () => {
            requestConfig.responseHandler = jasmine.createSpy('mockResponseHandler');

            return expectToReject(makeSuccessfulRequest())
                .then(() => {
                    expect(requestConfig.responseHandler).not.toHaveBeenCalled();
                });
        });

        it('rejects with the result of errorResponseHandler', () => {
            const expectedResponse = {some: 'object'};
            requestConfig.errorResponseHandler = () => Promise.resolve(expectedResponse);

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toEqual('UnexpectedHttpResponseCodeError');
                    expect(err.name).toBe('UnexpectedHttpResponseCodeError');
                    expect(err.message).toMatch(/returned unexpected HTTP response code/);
                    expect(err.response).toBe(expectedResponse);
                });
        });

        it('rejects if responseValidator throws', () => {
            requestConfig.responseValidator = () => {
                throw expectedError;
            };

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

        it('rejects if the errorResponseHandler rejects', () => {
            requestConfig.errorResponseHandler = () => Promise.reject(expectedError);

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

        it('rejects if the errorResponseHandler throws', () => {
            requestConfig.errorResponseHandler = () => {
                throw expectedError;
            };

            return expectToReject(makeSuccessfulRequest())
                .then((err) => {
                    expect(err.constructor.name).toBe('UnknownHttpleaseError');
                    expect(err.name).toBe('UnknownHttpleaseError');
                    expect(err.cause).toBe(expectedError);
                });
        });

    }); // end errorResponseHandler

});
