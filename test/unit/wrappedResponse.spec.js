'use strict';

const concatStream = require('concat-stream');
const crypto = require('crypto');
const events = require('events');
const stream = require('stream');
const util = require('util');
const zlib = require('zlib');

const WrappedResponse = require('../../lib/wrappedResponse').WrappedResponse;

describe('unit/wrappedResponse', () => {

    function createStubHttpResponse() {
        const httpResponse = new events.EventEmitter();
        httpResponse.pause = jasmine.createSpy('spyPause');
        httpResponse.destroy = jasmine.createSpy('spyDestroy');
        httpResponse.resume = jasmine.createSpy('spyResume');
        httpResponse.pipe = jasmine.createSpy('spyPipe');
        httpResponse.statusCode = 42;
        httpResponse.headers = {
            someheader: 'SomeValue'
        };
        return httpResponse;
    }

    function createStreamHttpResponse(buffer) {
        const httpResponse = new stream.PassThrough({highWaterMark: 0});
        httpResponse.destroy = jasmine.createSpy('spyDestroy');
        httpResponse.statusCode = 42;
        httpResponse.headers = {
            someheader: 'SomeValue'
        };

        if (buffer !== undefined) {
            httpResponse.write(buffer);
            httpResponse.end();
        }

        return httpResponse;
    }

    function expectDestroyedWithOverflowError(httpResponse) {
        expect(httpResponse.destroy).toHaveBeenCalled();
        const error = httpResponse.destroy.calls.argsFor(0)[0];
        expect(error.constructor.name).toBe('OverflowResponseBodyError');
        expect(error.name).toBe('OverflowResponseBodyError');
        expect(error.message).toBe('Overflow response body');
    }

    describe('constructor', () => {

        let httpResponse;

        beforeEach(() => {
            httpResponse = createStubHttpResponse();
        });

        it('pauses the response', () => {
            const wrappedResponse = new WrappedResponse(httpResponse); // eslint-disable-line no-unused-vars

            expect(httpResponse.pause).toHaveBeenCalledWith();
        });

        it('sets statusCode', () => {
            const wrappedResponse = new WrappedResponse(httpResponse);

            expect(wrappedResponse.statusCode).toBe(42);
        });

        it('sets headers', () => {
            const wrappedResponse = new WrappedResponse(httpResponse);

            expect(wrappedResponse.headers).toBe(httpResponse.headers);
        });

        it('makes headers available case insensitively through getHeader()', () => {
            const wrappedResponse = new WrappedResponse(httpResponse);

            expect(wrappedResponse.getHeader('SOMEHEADER')).toBe('SomeValue');
        });

    }); // end constructor

    describe('bufferBody', () => {

        it('fails on error', () => {
            const expectedError = new Error('dummy error');
            const httpResponse = createStubHttpResponse();
            setImmediate(() => httpResponse.emit('error', expectedError));

            const promise = new WrappedResponse(httpResponse).bufferBody();

            return expectToReject(promise)
                .then((err) => {
                    expect(err).toBe(expectedError);
                });
        });


        it('fails on buffer overflow', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('a'));

            return new WrappedResponse(httpResponse).bufferBody({maxSize: 0})
                .then(() => {
                    expectDestroyedWithOverflowError(httpResponse);
                });
        });

        it('fails on buffer overflow with default maxSize', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('a'.repeat(256*1024 + 1)));

            return new WrappedResponse(httpResponse).bufferBody()
                .then(() => {
                    expectDestroyedWithOverflowError(httpResponse);
                });
        });

        it('accepts maxSize instead of opts for compatibility', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('a'));

            return new WrappedResponse(httpResponse).bufferBody(0)
                .then(() => {
                    expectDestroyedWithOverflowError(httpResponse);
                });
        });

        it('handles empty buffer', () => {
            const httpResponse = createStreamHttpResponse();
            httpResponse.end();

            return new WrappedResponse(httpResponse).bufferBody()
                .then((buffer) => {
                    expect(buffer.length).toBe(0);
                });
        });

        it('handles single chunk', () => {
            const httpResponse = createStreamHttpResponse();
            httpResponse.write(Buffer.from('abc'));
            httpResponse.end();

            return new WrappedResponse(httpResponse).bufferBody()
                .then((buffer) => {
                    expect(buffer.toString('ascii')).toBe('abc');
                });
        });

        it('returns multiple chunks in one buffer', () => {
            const httpResponse = createStreamHttpResponse();
            httpResponse.write(Buffer.from('abc'));
            httpResponse.write(Buffer.from('def'));
            httpResponse.write(Buffer.from('123'));
            httpResponse.end();

            return new WrappedResponse(httpResponse).bufferBody()
                .then((buffer) => {
                    expect(buffer.toString('ascii')).toBe('abcdef123');
                });
        });

    }); // end bufferBody

    describe('bufferJson', () => {

        it('fails with invalid json', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('abcdef'));

            const promise = new WrappedResponse(httpResponse).bufferJson();

            return expectToReject(promise)
                .then((err) => {
                    expect(err.message).toMatch(/^Unexpected token/);
                });
        });

        it('returns valid json', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('{"abc":"def"}'));

            return new WrappedResponse(httpResponse).bufferJson()
                .then((body) => {
                    expect(body).toEqual({abc: 'def'});
                });
        });

        it('returns undefined for an empty body', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from(''));

            return new WrappedResponse(httpResponse).bufferJson()
                .then((body) => {
                    expect(body).toBeUndefined();
                });
        });

        it('fails on buffer overflow', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('a'));

            return new WrappedResponse(httpResponse).bufferJson({maxSize: 0})
                .then(() => {
                    expectDestroyedWithOverflowError(httpResponse);
                });
        });

        it('accepts maxSize instead of opts for compatibility', () => {
            const httpResponse = createStreamHttpResponse('a');

            return new WrappedResponse(httpResponse).bufferJson(0)
                .then(() => {
                    expectDestroyedWithOverflowError(httpResponse);
                });
        });

    }); // end bufferJson

    describe('digest', () => {

        it('returns the correct hash', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('abcdef'));

            return new WrappedResponse(httpResponse).digest({algorithm: 'sha1'})
                .then((result) => {
                    expect(result.toString('hex')).toBe('1f8ac10f23c5b5bc1167bda84b833e5c057a77d2');
                });
        });

        it('accepts algorithm as a string instead of opts for compatibility', () => {
            const httpResponse = createStreamHttpResponse(Buffer.from('abcdef'));

            return new WrappedResponse(httpResponse).digest('sha1')
                .then((result) => {
                    expect(result.toString('hex')).toBe('1f8ac10f23c5b5bc1167bda84b833e5c057a77d2');
                });
        });

    }); // end digest

    describe('pipe', () => {

        let httpResponse;

        beforeEach(() => {
            httpResponse = createStubHttpResponse();
        });

        it('resolves with wrapped Response when the response ends', () => {
            setImmediate(() => httpResponse.emit('end', 'ignored value'));

            const wrappedResponse = new WrappedResponse(httpResponse);

            return wrappedResponse.pipe()
                .then((result) => {
                    expect(result).toBe(wrappedResponse);
                });
        });

        it('rejects if the response has an error', () => {
            const expectedError = new Error('dummy error');
            setImmediate(() => httpResponse.emit('error', expectedError));

            const promise = new WrappedResponse(httpResponse).pipe();

            return expectToReject(promise)
                .then((err) => {
                    expect(err).toBe(expectedError);
                });
        });

        it('pipes data from the response into the param', () => {
            const fakeInto = {};
            const fakeOpts = {};

            new WrappedResponse(httpResponse).pipe(fakeInto, fakeOpts);

            expect(httpResponse.pipe).toHaveBeenCalledWith(fakeInto, fakeOpts);
        });

    }); // end pipe

    describe('discard', () => {

        let httpResponse;

        beforeEach(() => {
            httpResponse = createStubHttpResponse();
        });

        it('resolves with wrapped Response when the response ends', () => {
            setImmediate(() => httpResponse.emit('end', 'ignored value'));

            const wrappedResponse = new WrappedResponse(httpResponse);

            return wrappedResponse.discard()
                .then((result) => {
                    expect(result).toBe(wrappedResponse);
                });
        });

        it('rejects if the response has an error', () => {
            const expectedError = new Error('dummy error');
            setImmediate(() => httpResponse.emit('error', expectedError));

            const promise = new WrappedResponse(httpResponse).discard();

            return expectToReject(promise)
                .then((err) => {
                    expect(err).toBe(expectedError);
                });
        });

        it('resumes the response', () => {
            new WrappedResponse(httpResponse).discard();

            expect(httpResponse.resume).toHaveBeenCalledWith();
        });

    }); // end discard

    describe('getStandardResponse', () => {

        let httpResponse;

        beforeEach(() => {
            httpResponse = createStubHttpResponse();
        });

        it('returns an object with the correct shape', () => {
            const body = 'the body';
            const standardResponse = new WrappedResponse(httpResponse).getStandardResponse(body);

            expect(standardResponse).toEqual({
                statusCode: 42,
                headers: {
                    someheader: 'SomeValue'
                },
                getHeader: jasmine.any(Function),
                body
            });
        });

        it('has a working getHeader()', () => {
            const standardResponse = new WrappedResponse(httpResponse).getStandardResponse();

            expect(standardResponse.getHeader('SOMEHEADER')).toBe('SomeValue');
        });

        describe('with binary data in buffer body', () => {

            let standardResponse;

            beforeEach(() => {
                const bytes = [116, 101, 120, 116, 32, 97, 110, 100, 32, 98, 105, 110, 97, 114, 121, 32, 0, 127, 255, 32, 101, 110, 100]; // eslint-disable-line max-len
                standardResponse = new WrappedResponse(httpResponse).getStandardResponse(Buffer.from(bytes));
            });

            it('JSON.stringify gives a sensible format', () => {
                const json = JSON.stringify(standardResponse);
                expect(json).toContain('"body":"text and binary ??? end"');
            });

            it('console.log gives a sensible format', () => {
                const string = util.inspect(standardResponse);
                expect(string).toContain("body: 'text and binary ??? end'");
            });

        }); // end 'with binary data'


    }); // end 'getStandardResponse'

    describe('compression', () => {

        const ORIGINAL_BUFFER = Buffer.from(`{"key": "this will be compressed ${'!'.repeat(1024)}"}`);
        const GZIP_COMPRESSED_BUFFER = zlib.gzipSync(ORIGINAL_BUFFER); // eslint-disable-line no-sync
        const DEFLATE_COMPRESSED_BUFFER = zlib.gzipSync(ORIGINAL_BUFFER); // eslint-disable-line no-sync

        function createCompressedWrappedResponse(contentEncoding='gzip') {
            let buffer = ORIGINAL_BUFFER;
            if (contentEncoding === 'gzip') {
                buffer = GZIP_COMPRESSED_BUFFER;
            } else if (contentEncoding === 'deflate') {
                buffer = DEFLATE_COMPRESSED_BUFFER;
            }

            const httpResponse = createStreamHttpResponse(buffer);
            httpResponse.headers['content-encoding'] = contentEncoding;

            return new WrappedResponse(httpResponse);
        }

        function checkDigestDecompress(wrappedResponse) {
            return wrappedResponse.digest({algorithm: 'sha1', decompress: true})
                .then((result) => {
                    expect(result.toString('hex')).toBe('47c25686e0421d8bd32212bb687bd2cd37ca07ee');
                });
        }

        it('digest decompresses "gzip"', () => {
            const wrappedResponse = createCompressedWrappedResponse('gzip');
            return checkDigestDecompress(wrappedResponse);
        });

        it('digest decompresses "deflate"', () => {
            const wrappedResponse = createCompressedWrappedResponse('deflate');
            return checkDigestDecompress(wrappedResponse);
        });

        it('digest does not decompress "identity"', () => {
            const wrappedResponse = createCompressedWrappedResponse('identity');
            return checkDigestDecompress(wrappedResponse);
        });

        it('digest does not decompress by default', () => {
            const wrappedResponse = createCompressedWrappedResponse('gzip');
            const expectedHash = crypto.createHash('sha1').update(GZIP_COMPRESSED_BUFFER).digest('hex');

            return wrappedResponse.digest({algorithm: 'sha1'})
                .then((result) => {
                    expect(result.toString('hex')).toBe(expectedHash);
                });
        });

        it('digest treats unknown encoding as identity', () => {
            const wrappedResponse = createCompressedWrappedResponse('unknown-encoding');
            return checkDigestDecompress(wrappedResponse);
        });

        it('bufferBody decompresses when asked', () => {
            const wrappedResponse = createCompressedWrappedResponse();

            return wrappedResponse.bufferBody({decompress: true})
                .then((body) => {
                    expect(body).toEqual(ORIGINAL_BUFFER);
                });
        });

        it('bufferBody does not decompress by default', () => {
            const wrappedResponse = createCompressedWrappedResponse();

            return wrappedResponse.bufferBody()
                .then((body) => {
                    expect(body).toEqual(GZIP_COMPRESSED_BUFFER);
                });
        });

        it('bufferBody treats unknown encoding as identity', () => {
            const wrappedResponse = createCompressedWrappedResponse('unknown-encoding');

            return wrappedResponse.bufferBody({decompress: false})
                .then((body) => {
                    expect(body).toEqual(ORIGINAL_BUFFER);
                });
        });

        it('bufferJson decompresses', () => {
            const wrappedResponse = createCompressedWrappedResponse();

            return wrappedResponse.bufferJson()
                .then((body) => {
                    expect(body.key).toBeDefined();
                });
        });

        it('pipe decompresses when asked', () => {
            const wrappedResponse = createCompressedWrappedResponse();

            let savedBuffer;
            return wrappedResponse
                .pipe(concatStream((buffer) => {
                    savedBuffer = buffer;
                }), {decompress: true})
                .then(() => {
                    expect(savedBuffer).toEqual(ORIGINAL_BUFFER);
                });
        });

        it('pipe does not decompress by default', () => {
            const wrappedResponse = createCompressedWrappedResponse();

            let savedBuffer;
            return wrappedResponse
                .pipe(concatStream((buffer) => {
                    savedBuffer = buffer;
                }))
                .then(() => {
                    expect(savedBuffer).toEqual(GZIP_COMPRESSED_BUFFER);
                });
        });

        it('pipe treats unknown encoding as identity', () => {
            const wrappedResponse = createCompressedWrappedResponse('unknown-encoding');

            let savedBuffer;
            return wrappedResponse
                .pipe(concatStream((buffer) => {
                    savedBuffer = buffer;
                }), {decompress: true})
                .then(() => {
                    expect(savedBuffer).toEqual(ORIGINAL_BUFFER);
                });
        });

        it('rejects if the response has an error', () => {
            const wrappedResponse = createCompressedWrappedResponse();
            const expectedError = new Error('dummy error');
            setImmediate(() => wrappedResponse._httpResponse.emit('error', expectedError));

            return expectToReject(wrappedResponse.bufferBody({decompress: true}))
                .then((err) => {
                    expect(err).toBe(expectedError);
                });
        });

    }); // end 'compression'



});
