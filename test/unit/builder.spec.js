'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
const events = require('events');
const sendRequestHelpers = require('../../lib/sendRequest');
const responseHandlerHelpers = require('../../lib/responseHandler');
const {populateHttpOptions} = require('../../lib/populateHttpOptions');

describe('builder', () => {
    let mockRequest;
    let mockStream;
    let mockExecutePipeline;
    let mockResponseHandlerHelpers;
    let requestConfig;
    let Builder;
    let builder;

    beforeEach(() => {
        requestConfig = {};

        mockRequest = jasmine.createSpyObj('mockRequest', ['end', 'emit']);
        mockStream = new events.EventEmitter();
        mockStream.pipe = jasmine.createSpy('mockStreamPipe');

        mockExecutePipeline = jasmine.createSpy('executePipeline');
        mockExecutePipeline.and.returnValue(Promise.resolve(null));

        mockResponseHandlerHelpers = jasmine.createSpyObj('responseHandler', [
            'defaultErrorResponseHandler',
            'bufferJsonResponseHandler',
            'bufferBodyResponseHandler',
            'discardBodyResponseHandler'
        ]);

        Builder = requireWithMocks('../../lib/builder', {
            './pipeline': {executePipeline: mockExecutePipeline},
            './responseHandler': mockResponseHandlerHelpers
        }).Builder;
        builder = () => new Builder();
    });

    function getActualConfig() {
        return mockExecutePipeline.calls.argsFor(0)[0];
    }

    function expectConfigToContain(expectedConfig) {
        const actualConfig = getActualConfig();
        expect(actualConfig).toEqual(jasmine.objectContaining(expectedConfig));
    }

    describe('method', () => {

        it('should set GET method', () => {
            return builder().withMethodGet().send()
                .then(() => {
                    expectConfigToContain({method: 'GET'});
                });
        });

        it('should set POST method', () => {
            return builder().withMethodPost().send()
                .then(() => {
                    expectConfigToContain({method: 'POST'});
                });
        });

        it('should set PUT method', () => {
            return builder().withMethodPut().send()
                .then(() => {
                    expectConfigToContain({method: 'PUT'});
                });
        });

        it('should set DELETE method', () => {
            return builder().withMethodDelete().send()
                .then(() => {
                    expectConfigToContain({method: 'DELETE'});
                });
        });

        it('should set PATCH method', () => {
            return builder().withMethodPatch().send()
                .then(() => {
                    expectConfigToContain({method: 'PATCH'});
                });
        });

    }); // end method


    describe('URL', () => {

        it('should set base URL', () => {
            return builder().withBaseUrl('http://localhost').send()
                .then(() => {
                    expectConfigToContain({baseUrl: 'http://localhost'});
                });
        });

        it('should set path', () => {
            return builder().withPath('some/path').send()
                .then(() => {
                    expectConfigToContain({path: 'some/path'});
                });
        });

        it('should set params case sensitively', () => {
            return builder().withParams({SOME: 'param'}).send()
                .then(() => {
                    expectConfigToContain({
                        params: {
                            SOME: 'param'
                        }
                    });
                });
        });

        it('should extend params', () => {
            return builder().withParams({some: 'param'}).withParams({other: 'param2'}).send()
                .then(() => {
                    expectConfigToContain({
                        params: {
                            some: 'param',
                            other: 'param2'
                        }
                    });
                });
        });

        it('should extend params arrays', () => {
            return builder().withParams({multi: ['param1']}).withParams({multi: ['param2']}).send()
                .then(() => {
                    expectConfigToContain({
                        params: {
                            multi: ['param1', 'param2']
                        }
                    });
                });
        });

        it('should clear a param', () => {
            return builder().withParams({some: 'param'}).withParams({some: null}).send()
                .then(() => {
                    expectConfigToContain({
                        params: {}
                    });
                });
        });

        it('should copy params arrays', () => {
            const params = {multi: []};

            const client = builder().withParams(params);
            client.config.params.multi.push('this should not modify the original array');

            expect(params.multi).toEqual([]);
        });

    }); // end URL

    describe('headers', () => {

        it('should set headers as lower case', () => {
            return builder().withHeaders({SOME: 'header'}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            some: 'header'
                        }
                    });
                });
        });

        it('should extend headers', () => {
            return builder().withHeaders({some: 'header'}).withHeaders({other: 'header2'}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            some: 'header',
                            other: 'header2'
                        }
                    });
                });
        });

        it('should extend header arrays', () => {
            return builder().withHeaders({multi: ['header1']}).withHeaders({multi: ['header2']}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            multi: ['header1', 'header2']
                        }
                    });
                });
        });

        it('should clear a header', () => {
            return builder().withHeaders({some: 'param'}).withHeaders({some: null}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {}
                    });
                });
        });

        it('should copy header arrays', () => {
            const headers = {multi: []};

            const client = builder().withHeaders(headers);
            client.config.headers.multi.push('this should not modify the original array');

            expect(headers.multi).toEqual([]);
        });

    }); // end headers

    describe('request body', () => {

        it('sends an empty request by default', () => {
            return builder(requestConfig).send()
                .then(() => {
                    const sendRequestFn = getActualConfig().sendRequestFn;

                    sendRequestFn(mockRequest);

                    expect(mockRequest.end).toHaveBeenCalledWith();
                });
        });

        it('should set headers for JSON body: Content-Type and Content-Length', () => {
            return builder().withJsonBody({some: 'body 日本語'}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            'content-type': 'application/json',
                            'content-length': 25
                        }
                    });
                });
        });

        it('should not override existing Content-Type header for JSON body', () => {
            return builder().withHeaders({'Content-Type': 'foo'}).withJsonBody({test: true}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            'content-type': 'foo',
                            'content-length': 13
                        }
                    });
                });
        });

        it('should allow overriding Content-Type header with JSON body', () => {
            return builder().withJsonBody({some: 'body'}).withHeaders({'Content-Type': 'foobar'}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            'content-type': 'foobar',
                            'content-length': 15
                        }
                    });
                });
        });

        it('should send JSON body to request', () => {
            return builder().withJsonBody({some: 'body'}).send()
                .then(() => {
                    getActualConfig().sendRequestFn(mockRequest);
                    expect(mockRequest.end).toHaveBeenCalledWith(Buffer.from('{"some":"body"}'));
                });
        });

        it('should set header for buffer body: Content-Length', () => {
            return builder().withBufferBody(Buffer.from('abcdef')).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            'content-length': 6
                        }
                    });
                });
        });

        it('should send buffer body to request', () => {
            return builder().withBufferBody(Buffer.from('abcdef')).send()
                .then(() => {
                    getActualConfig().sendRequestFn(mockRequest);
                    expect(mockRequest.end).toHaveBeenCalledWith(Buffer.from('abcdef'));
                });
        });

        it('should set no headers with stream body', () => {
            return builder().withStreamBody({}).send()
                .then(() => {
                    expectConfigToContain({
                        headers: {}
                    });
                });
        });

        it('should pipe stream body to request', () => {
            return builder().withStreamBody(mockStream).send()
                .then(() => {
                    getActualConfig().sendRequestFn(mockRequest);
                    expect(mockStream.pipe).toHaveBeenCalledWith(mockRequest);
                });
        });

        it('should forward errors from stream body to request', () => {
            const expectedError = new Error('fake error');

            return builder().withStreamBody(mockStream).send()
                .then(() => {
                    getActualConfig().sendRequestFn(mockRequest);
                    mockStream.emit('error', expectedError);
                    expect(mockRequest.emit).toHaveBeenCalledWith('error', expectedError);
                });
        });

    }); // end request body

    describe('response handling', () => {

        it('should set responseValidator', () => {
            const responseValidator = jasmine.createSpy('spyResponseValidator');
            const fakeResponse = {fakeResponse: 'fake'};

            return builder().withResponseValidator(responseValidator).send()
                .then(() => {
                    getActualConfig().responseValidator(fakeResponse);
                    expect(responseValidator).toHaveBeenCalledWith(fakeResponse);
                });
        });

        it('should set expectStatus responseValidator', () => {
            return builder().withExpectStatus([200, 201]).send()
                .then(() => {
                    const responseValidator = getActualConfig().responseValidator;
                    expect(responseValidator({statusCode: 200})).toBe(true);
                    expect(responseValidator({statusCode: 201})).toBe(true);
                    expect(responseValidator({statusCode: 202})).toBe(false);
                });
        });

        it('should set responseHandler', () => {
            function someFunction() {}
            return builder().withResponseHandler(someFunction).send()
                .then(() => {
                    expectConfigToContain({
                        responseHandler: someFunction
                    });
                });
        });

        it('should set bufferJsonResponseHandler', () => {
            const opts = {foo: 42};

            return builder().withBufferJsonResponseHandler(opts).send()
                .then(() => {
                    const responseHandler = getActualConfig().responseHandler;
                    responseHandler('response');
                    expect(mockResponseHandlerHelpers.bufferJsonResponseHandler).toHaveBeenCalledWith('response', opts);
                });
        });

        it('should set bufferBodyResponseHandler', () => {
            const opts = {foo: 42};

            return builder().withBufferBodyResponseHandler(opts).send()
                .then(() => {
                    const responseHandler = getActualConfig().responseHandler;
                    responseHandler('response');
                    expect(mockResponseHandlerHelpers.bufferBodyResponseHandler).toHaveBeenCalledWith('response', opts);
                });
        });

        it('should set discardBodyResponseHandler', () => {
            return builder().withDiscardBodyResponseHandler().send()
                .then(() => {
                    expectConfigToContain({
                        responseHandler: mockResponseHandlerHelpers.discardBodyResponseHandler
                    });
                });
        });

        it('should set errorResponseHandler', () => {
            function someFunction() {}
            return builder().withErrorResponseHandler(someFunction).send()
                .then(() => {
                    expectConfigToContain({
                        errorResponseHandler: someFunction
                    });
                });
        });

    }); // end response handling

    describe('general config', () => {

        it('should pass the default config to request pipeline function', () => {
            return builder(requestConfig).send()
                .then(() => {
                    expect(mockExecutePipeline).toHaveBeenCalledWith({
                        httpOptions: {},
                        filters: [populateHttpOptions],
                        headers: {},
                        params: {},
                        sendRequestFn: sendRequestHelpers.sendEmptyBody,
                        errorResponseHandler: responseHandlerHelpers.defaultErrorResponseHandler
                    });
                });
        });

        it('should set timeout', () => {
            return builder().withTimeout(4242).send()
                .then(() => {
                    expectConfigToContain({
                        timeout: 4242
                    });
                });
        });

        it('should set connect timeout', () => {
            return builder().withConnectTimeout(4111).send()
                .then(() => {
                    expectConfigToContain({
                        connectTimeout: 4111
                    });
                });
        });

        it('should set filters', () => {
            const someFilter = () => {};
            return builder().withFilter(someFilter).send()
                .then(() => {
                    expectConfigToContain({filters: [populateHttpOptions, someFilter]});
                });
        });

        it('should extend filters', () => {
            const someFilter = () => {};
            const otherFilter = () => {};
            return builder().withFilter(someFilter).withFilter(otherFilter).send()
                .then(() => {
                    expectConfigToContain({filters: [populateHttpOptions, someFilter, otherFilter]});
                });
        });

        it('should set httpOptions', () => {
            return builder().withHttpOptions({protocol: 'http:', hostname: 'example.com'}).send()
                .then(() => {
                    expectConfigToContain({
                        httpOptions: {
                            protocol: 'http:',
                            hostname: 'example.com'
                        }
                    });
                });
        });

        it('should provide helpful warning if withAgentOptions is called before withBaseUrl', () => {
            const fn = () => builder().withAgentOptions({keepAlive: true});
            expect(fn).toThrowError('You must call .withBaseUrl() before calling .withAgentOptions()');
        });

        it('should create http.Agent', () => {
            return builder().withBaseUrl('http://invalid').withAgentOptions({keepAlive: true}).send()
                .then(() => {
                    expectConfigToContain({
                        httpOptions: {
                            agent: jasmine.objectContaining({
                                protocol: 'http:',
                                keepAlive: true
                            })
                        }
                    });
                });
        });

        it('should create https.Agent', () => {
            return builder().withBaseUrl('https://invalid').withAgentOptions({keepAlive: true}).send()
                .then(() => {
                    expectConfigToContain({
                        httpOptions: {
                            agent: jasmine.objectContaining({
                                protocol: 'https:',
                                keepAlive: true
                            })
                        }
                    });
                });
        });

        it('should not copy the httpOptions.agent', () => {
            const client = builder().withBaseUrl('http://invalid').withAgentOptions({keepAlive: true});
            return client.withConfig({forceCopyToHappen: '...'}).send()
                .then(() => {
                    const agent = getActualConfig().httpOptions.agent;
                    expect(client.config.httpOptions.agent).toBe(agent);
                });
        });

        it('should set arbitrary config', () => {
            return builder().withConfig({arbitraryBlob: 'blobblob'}).send()
                .then(() => {
                    expectConfigToContain({arbitraryBlob: 'blobblob'});
                });
        });

        it('should accept arbitrary extraConfig in send()', () => {
            return builder().send({parentTrace: 'blobblob'})
                .then(() => {
                    expectConfigToContain({parentTrace: 'blobblob'});
                });
        });

        it('should create immutable requests', () => {
            const firstRequest = builder().withMethodDelete();
            const secondRequest = firstRequest.withParams({param: 'param'});

            return firstRequest.send()
                .then(() => {
                    expectConfigToContain({method: 'DELETE'});
                    mockExecutePipeline.calls.reset();
                    return secondRequest.send();
                })
                .then(() => {
                    expectConfigToContain({method: 'DELETE', params: {param: 'param'}});
                });
        });

        it('should set clientName', () => {
            return builder().withClientName('some-name').send()
                .then(() => {
                    expectConfigToContain({clientName: 'some-name'});
                });
        });

    }); // end general config

    describe('Builder sub-class', () => {

        let MyBuilder;

        beforeEach(() => {
            MyBuilder = class MyBuilder extends Builder {
                withSomething(something) {
                    return this.withConfig({something});
                }
                withMethodGet() {
                    return this.withConfig({funkyGet: true});
                }
            };
        });

        it('is exported through index', () => {
            const {Builder} = require('../../lib/index');
            const builder = new Builder().withMethodGet();
            expect(builder.config.method).toBe('GET');
        });

        it('can add a config method', () => {
            return new MyBuilder().withSomething('foo').send()
                .then(() => {
                    expectConfigToContain({something: 'foo'});
                });
        });

        it('can chain multiple methods', () => {
            return new MyBuilder().withSomething(['foo']).withSomething(['bar']).send()
                .then(() => {
                    expectConfigToContain({something: ['foo', 'bar']});
                });
        });

        it('works with base class methods', () => {
            return new MyBuilder()
                .withHeaders({header1: true})
                .withSomething(['foo'])
                .withHeaders({header2: true})
                .withSomething(['bar'])
                .send()
                .then(() => {
                    expectConfigToContain({
                        headers: {
                            header1: true,
                            header2: true
                        },
                        something: ['foo', 'bar']
                    });
                });
        });

        it('overrides base class methods', () => {
            return new MyBuilder().withMethodGet().send()
                .then(() => {
                    expectConfigToContain({funkyGet: true});
                });
        });


    }); // end subclassing

});
