'use strict';

const {populateHttpOptions} = require('../../lib/populateHttpOptions');

describe('populateHttpOptions', () => {
    let fakeResponse;
    let fakeNext;
    let requestConfig;

    beforeEach(() => {
        fakeResponse = 'fakeResponse';

        requestConfig = {
            httpOptions: {},
            headers: {}
        };

        fakeNext = jasmine.createSpy('next');
        fakeNext.and.returnValue(Promise.resolve(fakeResponse));
    });

    it('returns result of calling next()', () => {
        return populateHttpOptions(requestConfig, fakeNext)
            .then((response) => {
                expect(fakeNext).toHaveBeenCalledWith(requestConfig);
                expect(response).toBe(fakeResponse);
            });
    });

    it('sets blank object when no options are given', () => {
        return populateHttpOptions(requestConfig, fakeNext)
            .then(() => {
                expect(requestConfig.httpOptions).toEqual({
                    method: undefined,
                    protocol: null,
                    hostname: null,
                    port: null,
                    path: null,
                    headers: {}
                });
            });
    });

    describe('request with baseUrl/path/params', () => {

        beforeEach(() => {
            requestConfig.baseUrl = 'http://example.com/baseUrl';
            requestConfig.path = '/morePath';
            requestConfig.method = 'GET';
        });

        it('copies into httpOptions', () => {
            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual({
                        protocol: 'http:',
                        method: 'GET',
                        hostname: 'example.com',
                        port: null,
                        path: '/baseUrl/morePath',
                        headers: {}
                    });
                });
        });

        it('handles https protocol', () => {
            requestConfig.baseUrl = 'https://example.com/baseUrl';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.protocol).toBe('https:');
                });
        });

        it('appends params to the path in query string format', () => {
            requestConfig.params = {
                q: 'some value',
                multi: ['multi1', 'multi2']
            };

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.path).toBe(
                        '/baseUrl/morePath?q=some%20value&multi=multi1&multi=multi2'
                    );
                });
        });

        it('works with only path', () => {
            delete requestConfig.baseUrl;
            requestConfig.path = 'http://example.com/thePath';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual(jasmine.objectContaining({
                        protocol: 'http:',
                        hostname: 'example.com',
                        path: '/thePath'
                    }));
                });
        });

        it('works with only baseUrl', () => {
            delete requestConfig.path;
            requestConfig.baseUrl = 'http://example.com/thePath';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual(jasmine.objectContaining({
                        protocol: 'http:',
                        hostname: 'example.com',
                        path: '/thePath'
                    }));
                });
        });

        it('parses custom ports', () => {
            requestConfig.baseUrl = 'http://example.com:42/baseUrl';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual(jasmine.objectContaining({
                        hostname: 'example.com',
                        port: '42',
                        path: '/baseUrl/morePath'
                    }));
                });
        });

    }); // end 'request with baseUrl/path/params'

    describe('request with httpOptions', () => {

        beforeEach(() => {
            requestConfig.httpOptions = {
                protocol: 'http:',
                hostname: 'example.com',
                path: '/thePath',
                method: 'GET'
            };
        });

        it('does not touch httpOptions if they are set', () => {
            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual({
                        protocol: 'http:',
                        hostname: 'example.com',
                        path: '/thePath',
                        method: 'GET',
                        headers: {}
                    });
                });
        });

        it('does not overwrite method if it is specified in httpOptions', () => {
            requestConfig.method = 'IGNORED';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.method).toBe('GET');
                });
        });

        it('if protocol is specified then baseUrl/path/params are ignored', () => {
            requestConfig.baseUrl = 'this baseUrl is ignored';
            requestConfig.path = 'this path is ignored';
            requestConfig.params = {ignoredParam: 'this param is ignored'};

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions).toEqual({
                        protocol: 'http:',
                        hostname: 'example.com',
                        path: '/thePath',
                        method: 'GET',
                        headers: {}
                    });
                });
        });

    }); // end 'request with httpOptions'

    describe('headers', () => {

        beforeEach(() => {
            requestConfig.method = 'GET';
            requestConfig.baseUrl = 'http://example.com';
        });

        it('sends no headers by default', () => {
            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.headers).toEqual({});
                });
        });

        it('sends headers from httpOptions', () => {
            requestConfig.httpOptions.headers = {SomeHeader: 'some value'};

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.headers).toEqual({SomeHeader: 'some value'});
                });
        });

        it('sends headers from requestConfig', () => {
            requestConfig.headers.SomeHeader = 'some value';

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.headers).toEqual({SomeHeader: 'some value'});
                });
        });

        it('merges headers from requestConfig and httpOptions', () => {
            requestConfig.headers = {MultiHeader: ['some value1'], SomeHeader: 'ignored value'};
            requestConfig.httpOptions.headers = {MultiHeader: ['some value2'], SomeHeader: 'kept value'};

            return populateHttpOptions(requestConfig, fakeNext)
                .then(() => {
                    expect(requestConfig.httpOptions.headers).toEqual({
                        MultiHeader: ['some value1', 'some value2'],
                        SomeHeader: 'kept value'
                    });
                });
        });

    }); // end headers

});
