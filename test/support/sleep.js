'use strict';

module.exports = function sleep(timeoutMillis) {
    return new Promise((r) => {
        setTimeout(r, timeoutMillis);
    });
};
