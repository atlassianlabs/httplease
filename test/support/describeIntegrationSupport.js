'use strict';

const jasmineHttpSpy = require('jasmine-http-server-spy');
const httplease = require('../../lib/index');

function describeIntegrationSupport(description, fn) {
    describe('integration', () => {

        const support = {};

        beforeAll(() => {
            support.httpClient = httplease.builder()
                .withBaseUrl('http://localhost:8082')
                .withTimeout(5000);

            support.httpSpy = jasmineHttpSpy.createSpyObj('mockServer', [
                {
                    handlerName: 'getHandler',
                    method: 'get',
                    url: '/get'
                },
                {
                    handlerName: 'postHandler',
                    method: 'post',
                    url: '/post'
                }
            ]);

            return support.httpSpy.server.start(8082);
        });

        afterAll(() => {
            return support.httpSpy.server.stop();
        });

        afterEach(() => {
            support.httpSpy.getHandler.calls.reset();
            support.httpSpy.postHandler.calls.reset();
        });

        describe(description, () => {
            fn(support);
        });

    });
}

module.exports = describeIntegrationSupport;
