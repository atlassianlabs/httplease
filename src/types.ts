export interface RequestConfig {
    baseUrl?: string;
    clientName?: string;
    errorResponseHandler?: any;
    filters?: any[];
    headers?: any;
    httpOptions?: any;
    method?: string;
    params?: any;
    sendRequestFn?: any;
    path?: string;
    responseHandler?: any;
    responseValidator?: any;
    timeout?: number;
    connectTimeout?: number
}

export interface ResponseHandlerOptions {
    algorithm?: string;
    decompress?: boolean;
    maxSize?: number;
}
