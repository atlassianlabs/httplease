import {RequestConfig} from './types';

export class HttpleaseError extends Error {
    clientName?: string;

    constructor(msg, options?) {
        // @ts-ignore
        super(msg, options);
        this.name = this.constructor.name;
    }
}

export class InvalidHttpleaseConfigError extends HttpleaseError { }

export class UnknownHttpleaseError extends HttpleaseError {
    constructor(requestConfig: RequestConfig, cause: Error) {
        super(
            `Request (${stringifyRequestConfig(requestConfig)}) ` +
                'has failed with an unknown error: ' + cause.message,
            {cause}
        );
    }
}

export class UnexpectedHttpResponseCodeError extends HttpleaseError {
    response: any;

    constructor(requestConfig: RequestConfig, response) {
        super(`Request (${stringifyRequestConfig(requestConfig)}) ` +
              `returned unexpected HTTP response code: ${response.statusCode}`);
        this.response = response;
    }
}

export class TimeoutError extends HttpleaseError {
    constructor(requestConfig: RequestConfig, message?: string) {
        if (message) {
            super(message);
        } else {
            super(`Request (${stringifyRequestConfig(requestConfig)}) ` +
                `has timed out after ${requestConfig.timeout}msec`);
        }
    }
}

export class ConnectTimeoutError extends TimeoutError {
    constructor(requestConfig: RequestConfig, connectTimeout: number) {
        super(
            requestConfig,
            `Request (${stringifyRequestConfig(requestConfig)}) ` +
                `has timed out after ${connectTimeout}msec while trying to connect`
        );
    }
}

export class NotJsonContentTypeError extends HttpleaseError {
    constructor(contentType) {
        super(`Response was not JSON! Content-Type: ${contentType}`);
    }
}

/** @deprecated unused, kept for compatibility */
export class UnknownContentEncodingError extends HttpleaseError { }

export class OverflowResponseBodyError extends HttpleaseError {
    constructor() {
        super('Overflow response body');
    }
}

function stringifyRequestConfig(requestConfig) {
    return ['method', 'protocol', 'hostname', 'host', 'port', 'path']
        .map((key) => {
            const value = requestConfig.httpOptions[key];
            if (!value) {
                return null;
            }
            return `${key}=${value}`;
        })
        .filter((s) => s !== null)
        .join(';');
}
