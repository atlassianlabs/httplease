import * as url from 'url';

import {mergeConfig} from './utils/mergeConfig';

import {RequestConfig} from './types';

function constructUrl(requestConfig: RequestConfig) {
    return (requestConfig.baseUrl || '') + (requestConfig.path || '');
}

function setUrlParamsFromPath(requestConfig: RequestConfig, httpOptions) {
    const urlString = constructUrl(requestConfig) + url.format({query: requestConfig.params});

    const urlObj = url.parse(urlString);
    httpOptions.protocol = urlObj.protocol;
    httpOptions.hostname = urlObj.hostname;
    httpOptions.port = urlObj.port;
    httpOptions.path = urlObj.path;
}

export function populateHttpOptions(requestConfig: RequestConfig, next) {
    const httpOptions = Object.assign({}, requestConfig.httpOptions);

    if (!httpOptions.method) {
        httpOptions.method = requestConfig.method;
    }

    if (!httpOptions.protocol) {
        setUrlParamsFromPath(requestConfig, httpOptions);
    }

    httpOptions.headers = mergeConfig(requestConfig.headers, httpOptions.headers);

    requestConfig.httpOptions = httpOptions;

    return next(requestConfig);
}
