import * as errors from './errors';
import {Builder} from './builder';
import {RequestConfig} from './types';

function builder(requestConfig?: RequestConfig) {
    return new Builder(requestConfig);
}

export {
    builder,
    Builder,
    errors
};
export * from './errors';
export * from './types';
