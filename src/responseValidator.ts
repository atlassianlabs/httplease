export function createExpectStatusValidator(expectStatus: number[]) {
    return function expectStatusValidator(wrappedResponse) {
        return expectStatus.indexOf(wrappedResponse.statusCode) >= 0;
    };
}
