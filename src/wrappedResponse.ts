import * as crypto from 'crypto';
import * as util from 'util';
import * as zlib from 'zlib';

import {isJson} from './utils/contentType';
import {ResponseHandlerOptions} from './types';
import {OverflowResponseBodyError} from './errors';
import {OutgoingHttpHeaders} from 'http';

const DEFAULT_MAX_BUFFER_SIZE = 256 * 1024;

export class WrappedResponse {
    statusCode: number;
    headers: OutgoingHttpHeaders;
    body?: Buffer;
    _httpResponse: any;

    constructor(httpResponse) {
        httpResponse.pause();

        this.statusCode = httpResponse.statusCode;

        this.headers = httpResponse.headers;

        this._httpResponse = httpResponse;
    }

    _getStream(decompress) {
        if (decompress) {
            return this._getDecompressedStream();
        } else {
            return this._httpResponse;
        }
    }

    _getDecompressedStream() {
        const contentEncoding = this.headers['content-encoding'];
        if (contentEncoding === 'gzip' || contentEncoding === 'deflate') {
            const unzipper = zlib.createUnzip();
            this._httpResponse.on('error', (err) => unzipper.emit('error', err));
            this._httpResponse.pipe(unzipper);
            return unzipper;
        } else if (contentEncoding === 'identity' || !contentEncoding) {
            return this._httpResponse;
        } else {
            // Servers will mistakenly send things like `Content-Encoding: UTF8`.
            // It's very unlikely that a server will start sending some new
            // compression encoding like ZSTD without us sending `Accept-Encoding: ZSTD`
            // so it's better to silently ignore invalid values.
            return this._httpResponse;
        }
    }

    /**
      * Returns a header from the response.
      */
    getHeader(name) : string {
        return this._httpResponse.headers[name.toLowerCase()];
    }

    /**
      * Returns true if the response has a body, false otherwise.
      *
      * This checks for statusCode==204 or Content-Length==0.
      */
    hasBody() {
        if (this.statusCode === 204) {
            return false;
        }
        if (parseInt(this.getHeader('Content-Length'), 10) === 0) {
            return false;
        }
        return true;
    }

    /**
      * Returns true if the Content-Type of the response is JSON.
      */
    isContentTypeJson() {
        return isJson(this.getHeader('Content-Type'));
    }

    /**
     * Returns an object with the response details in the standard format.
     *
     * The responseHandler should return this simplified object rather than
     * the WrappedResponse.
     */
    getStandardResponse(body) {
        const response = {
            statusCode: this.statusCode,
            headers: this.headers,
            getHeader: this.getHeader.bind(this),
            body
        };

        if (body instanceof Buffer) {
            attachBufferPrettyPrinters(response);
        }

        return response;
    }

    /**
     * Reads the response body and resolves with that buffer.
     *
     * This method can only be called from within the responseHandler.
     *
     * @param {boolean} opts.decompress - decompress gzip/deflate response bodies
     * @param {number} opts.maxSize - (defaults to 256KiB)
     */
    bufferBody(_opts: ResponseHandlerOptions) : Promise<Buffer> {
        const opts = parseBufferOpts(_opts);

        const chunks: any[] = [];
        let byteCount = 0;
        return new Promise((resolve, reject) => {
            const responseStream = this._getStream(opts.decompress);
            responseStream.once('error', reject);
            responseStream.once('end', () => {
                resolve(Buffer.concat(chunks));
            });
            responseStream.on('data', (chunk) => {
                if (byteCount + chunk.length > opts.maxSize) {
                    const error = new OverflowResponseBodyError();
                    return this._httpResponse.destroy(error);
                }
                byteCount += chunk.length;
                chunks.push(chunk);
            });
            responseStream.resume();
        });
    }

    /**
     * Reads the response body, parses it as JSON and resolves with that object.
     *
     * This method can only be called from within the responseHandler.
     *
     * @param {Object} opts
     * @param {number} opts.maxSize - (defaults to 256KiB)
     */
    bufferJson(_opts: ResponseHandlerOptions) : Promise<any> {
        const opts = parseBufferOpts(_opts);

        return this.bufferBody({maxSize: opts.maxSize, decompress: true})
            .then((buffer) => {
                if (buffer.length > 0) {
                    return JSON.parse(<any>buffer);
                }
            });
    }

    /**
     * Reads the response body, hashes it and resolves with a buffer of the digest.
     *
     * This method can only be called from within the responseHandler.
     *
     * @param {Object} opts
     * @param {string} opts.algorithm - OpenSSL hash algorithm, eg sha1, sha256, md5
     * @param {boolean} opts.decompress - decompress gzip/deflate response bodies
     */
    digest(_opts: ResponseHandlerOptions) : Promise<Buffer> {
        const opts = parseDigestOpts(_opts);

        return new Promise((resolve) => {
            const responseStream = this._getStream(opts.decompress);
            const hash = responseStream.pipe(crypto.createHash(opts.algorithm));

            hash.once('readable', () => {
                return resolve(hash.read());
            });
        });
    }

    /**
     * Pipes the response body into the provided stream.
     *
     * This method can only be called from within the responseHandler.
     *
     * @param {Object} opts
     * @param {boolean} opts.decompress - decompress gzip/deflate response bodies
     */
    pipe(into, opts: ResponseHandlerOptions = {}) : Promise<WrappedResponse> {
        return new Promise((resolve, reject) => {
            const responseStream = this._getStream(opts.decompress);
            responseStream.on('end', () => resolve(this));
            responseStream.on('error', reject);
            responseStream.pipe(into, opts);
        });
    }

    /**
     * Discards the response body.
     *
     * This method can only be called from within the responseHandler.
     */
    discard() : Promise<WrappedResponse> {
        return new Promise((resolve, reject) => {
            this._httpResponse.on('end', () => resolve(this));
            this._httpResponse.on('error', reject);
            this._httpResponse.resume();
        });
    }
}

function attachBufferPrettyPrinters(response) {
    Object.defineProperty(response, 'toJSON', {value: prettyPrintBufferBodyResponse});
    Object.defineProperty(response, 'inspect', {value: prettyPrintBufferBodyResponse});
    Object.defineProperty(response, util.inspect.custom, {value: prettyPrintBufferBodyResponse});
}

function prettyPrintBufferBodyResponse(this: WrappedResponse) {
    const self = this; // eslint-disable-line no-invalid-this
    const result = Object.assign({}, self) as any;
    result.body = Array.from(self.body!)
        .map((ch) => {
            if (ch > 31 && ch < 127) {
                return String.fromCharCode(ch);
            } else {
                return '?';
            }
        })
        .join('');
    return result;
}

function parseBufferOpts(opts: any = {}) {
    if (typeof opts === 'number') {
        return {maxSize: opts};
    }
    if (opts.maxSize === undefined) {
        opts.maxSize = DEFAULT_MAX_BUFFER_SIZE;
    }
    return opts;
}

function parseDigestOpts(opts) {
    if (typeof opts === 'string') {
        return {algorithm: opts};
    }

    return opts;
}
