export function convertKeysToLowerCase(mixedCaseObj) {
    const lowerObj = {};
    Object.keys(mixedCaseObj).forEach((key) => {
        lowerObj[key.toLowerCase()] = mixedCaseObj[key];
    });
    return lowerObj;
}
