export function isJson(contentType) { // eslint-disable-line complexity
    const p = parseContentType(contentType);

    return (p.topType === 'application' || p.topType === 'text') &&
        (p.subType === 'json' || p.subType.endsWith('+json')) &&
        (!p.parameters.charset || p.parameters.charset.toLowerCase() === 'utf-8');
}

export function parseContentType(contentType) {
    if (!contentType) {
        return newInvalidContentType();
    }

    const tokens = contentType
        .toLowerCase()
        .split(';')
        .map((s) => s.trim());

    const types = tokens[0].split('/');
    if (types.length !== 2) {
        return newInvalidContentType();
    }

    const parameters = parseParameters(tokens.slice(1));

    return {
        topType: types[0],
        subType: types[1],
        parameters
    };
}

function parseParameters(parameters) {
    return parameters
        .reduce((obj, keyValue) => {
            const [key, value] = keyValue.split('=');
            if (key !== undefined && value !== undefined) {
                obj[key.toLowerCase()] = value;
            }
            return obj;
        }, {});
}

function newInvalidContentType() {
    return {
        topType: 'invalid',
        subType: 'invalid',
        parameters: {}
    };
}
