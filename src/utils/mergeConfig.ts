function isArray(x) {
    return x && (x instanceof Array);
}

function isObject(x) {
    return x && (x.constructor === Object);
}

function deepMerge(dest, src) {
    Object.keys(src).forEach((key) => deepMergeOneKey(dest, src, key));
}

function deepMergeOneKey(dest, src, key) { // eslint-disable-line complexity
    if (isObject(src[key])) {
        if (!dest[key]) {
            dest[key] = {};
        }
        deepMerge(dest[key], src[key]);
    } else if (isArray(src[key]) && isArray(dest[key])) {
        dest[key] = dest[key].concat(src[key]);
    } else if (isArray(src[key])) {
        dest[key] = [].concat(src[key]);
    } else if (src[key] === null) {
        delete dest[key];
    } else {
        dest[key] = src[key];
    }
}

export function mergeConfig(baseConfig, extraConfig) {
    const result = {};
    deepMerge(result, baseConfig);
    deepMerge(result, extraConfig || {});
    return result;
}
