import * as http from 'http';
import * as https from 'https';

export function getClientForProtocol(protocol) {
    switch (protocol) {
    case 'http:':
        return http;
    case 'https:':
        return https;
    default:
        throw new Error(`No client for protocol '${protocol}'`);
    }
}
