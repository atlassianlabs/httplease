import * as responseHandlerHelpers from './responseHandler';
import * as responseValidatorHelpers from './responseValidator';
import * as sendRequestHelpers from './sendRequest';
import * as stream from 'stream';
import * as url from 'url';

import {convertKeysToLowerCase} from './utils/convertKeysToLowerCase';
import {defaultConfig} from './defaultConfig';
import {executePipeline} from './pipeline';
import {getClientForProtocol} from './utils/getClientForProtocol';
import {mergeConfig} from './utils/mergeConfig';

import {RequestConfig, ResponseHandlerOptions} from './types';

export class Builder {
    config: RequestConfig;

    constructor(baseConfig: RequestConfig = defaultConfig, config: RequestConfig = {}) {
        this.config = mergeConfig(baseConfig, config);
    }

    /** Set the HTTP method to GET */
    withMethodGet(): this {
        return this.withConfig({method: 'GET'});
    }
    /** Set the HTTP method to POST */
    withMethodPost(): this {
        return this.withConfig({method: 'POST'});
    }
    /** Set the HTTP method to PUT */
    withMethodPut(): this {
        return this.withConfig({method: 'PUT'});
    }
    /** Set the HTTP method to DELETE */
    withMethodDelete(): this {
        return this.withConfig({method: 'DELETE'});
    }
    /** Set the HTTP method to PATCH */
    withMethodPatch(): this {
        return this.withConfig({method: 'PATCH'});
    }

    /**
     * The base URL for the request.
     * @example withBaseUrl('http://example.com/path/to/foo')
     */
    withBaseUrl(baseUrl: string): this {
        return this.withConfig({baseUrl});
    }

    /**
     * Optional extra path component for the request.
     *
     * If this is present then baseUrl and path are concatenated as
     * `${baseUrl}${path}` to construct the final URL.
     *
     * @example withPath('/extra/path')
     */
    withPath(path: string): this {
        return this.withConfig({path});
    }

    /**
     * Optional query string parameters.
     *
     * If you supply an object and it will be formatted by url.format() then
     * appended to the URL.
     *
     * @example withParams({q: 'something', multi: ['foo', 'bar']})
     */
    withParams(params: {[k: string]: (string | string[])}): this {
        return this.withConfig({params});
    }

    /**
     * Set arbitrary headers as an object.
     *
     * Headers will all be normalised to lower case.
     *
     * @example: withHeaders({SomeHeader: 'some-value', 'Cookie': ['key1=value1', 'key2=value2']})
     */
    withHeaders(mixedCaseHeaders: {[k: string]: (string | string[])}): this {
        const headers = convertKeysToLowerCase(mixedCaseHeaders);
        return this.withConfig({headers});
    }

    /**
     * Set the request body as a JSON object.
     *
     * The content-length and content-type headers will be set automatically.
     */
    withJsonBody(body: unknown): this {
        return sendRequestHelpers.withJsonBody(this, body);
    }

    /**
     * Set the request body to a buffer.
     *
     * The content-length header will be set automatically.
     */
    withBufferBody(buffer: Buffer): this {
        return sendRequestHelpers.withBufferBody(this, buffer);
    }

    /**
     * Stream the provided request body to the server.
     *
     * The request will be sent to the server in chunks.
     * You must set the Content-Type header yourself.
     */
    withStreamBody(stream: stream.Readable): this {
        return sendRequestHelpers.withStreamBody(this, stream);
    }


    /**
     * Expect the response to have one of these statuses.
     *
     * If the status is not as expected then your responseHandler will not be
     * called. Instead the errorResponseHandler will be called.
     *
     * By default this will attempt JSON parsing and the promise will be rejected.
     *
     * @example withExpectStatus([200, 204])
     */
    withExpectStatus(expectStatus: number[]): this {
        return this.withResponseValidator(responseValidatorHelpers.createExpectStatusValidator(expectStatus));
    }

    /**
     * When the response is received, validate it with this function.
     *
     * If the validation fails then your responseHandler will not be called.
     * Instead the errorResponseHandler will be called.
     *
     * By default this will attempt JSON parsing and the promise will be
     * rejected.
     */
    withResponseValidator(responseValidator): this {
        return this.withConfig({responseValidator});
    }

    /**
     * Parse the response into a JSON object.
     *
     * The request will be resolved with an object like:
     *   {statusCode, headers, getHeader(), body: {}}
     *
     * @param {number} opts.maxSize - (defaults to 256KiB)
     */
    withBufferJsonResponseHandler(opts?: ResponseHandlerOptions): this {
        return this.withConfig({
            responseHandler: (response) => responseHandlerHelpers.bufferJsonResponseHandler(response, opts)
        });
    }

    /**
     * Store the response body in a Buffer object.
     *
     * The request will be resolved with an object like:
     *   {statusCode, headers, getHeader(), body: Buffer}
     *
     * @param {number} opts.maxSize - (defaults to 256KiB)
     * @param {boolean} opts.decompress - decompress gzip/deflate response bodies
     */
    withBufferBodyResponseHandler(opts?: ResponseHandlerOptions): this {
        return this.withConfig({
            responseHandler: (response) => responseHandlerHelpers.bufferBodyResponseHandler(response, opts)
        });
    }

    /**
     * Discard the response body.
     *
     * The request will be resolved with an object like:
     *   {statusCode, headers, getHeader(), body: null}
     */
    withDiscardBodyResponseHandler(): this {
        return this.withConfig({responseHandler: responseHandlerHelpers.discardBodyResponseHandler});
    }

    /**
     * Set a handler which is given access to the response object.
     *
     * This can be used to connect a stream to the HTTP response. If you do not
     * set this the default response handler resolve with an object like:
     *   {statusCode, headers, getHeader(), body: JSON}
     *
     * Your responseHandler will be called with a WrappedResponse object and
     * must return a promise. The resolved value of the promise will be the
     * value which is returned as the resolved value of builder.send().
     *
     * Your responseHandler must consume the response in some way, at minimum
     * you can WrappedResponse.discard() it.
     */
    withResponseHandler(responseHandler): this {
        return this.withConfig({responseHandler});
    }

    /**
     * Set an error response handler.
     *
     * This behaves the same as `responseHandler` but is only called if the
     * `responseValidator` fails.
     */
    withErrorResponseHandler(errorResponseHandler): this {
        return this.withConfig({errorResponseHandler});
    }

    /**
     * Set the socket timeout in milliseconds.
     *
     * See Node documentation for details on how the timeout works
     * https://nodejs.org/api/http.html#http_request_settimeout_timeout_callback
     */
    withTimeout(timeout: number | null): this {
        return this.withConfig({timeout});
    }

    /**
     * Set the connection timeout in milliseconds.
     *
     * This timeout triggers if a socket hasn't been assigned and connected within configured connect timeout
     * Note: It will also trigger if the Agent doesn't assign a socket to the request within the configured timeout
     */
    withConnectTimeout(connectTimeout: number | null): this {
        return this.withConfig({connectTimeout});
    }

    /**
     * Sets the name for this HTTP client.
     *
     * If set this property will be copied onto any errors returned by send().
     * Filters may also choose to use this value for any other purpose.
     */
    withClientName(clientName: string): this {
        return this.withConfig({clientName});
    }

    /**
     * Set a custom filter which has access to the request/responses.
     *
     * The filter is passed the requestConfig and the next filter in the
     * pipeline. Calling next(requestConfig) will return a promise which
     * resolves with the response object.
     *
     * The identity filter would look like this:
     *
     * function (requestConfig, next): this {
     *     return next(requestConfig);
     * }
     */
    withFilter(filter): this {
        return this.withConfig({filters: [filter]});
    }

    /**
     * Set options for the Node http/https module.
     *
     * These go straight through to the node http/https libraries.
     */
    withHttpOptions(httpOptions): this {
        return this.withConfig({httpOptions});
    }

    /**
     * Set options for the agent.
     *
     * These go straight through to the Node http.Agent/https.Agent class.
     *
     * Calling this function will create a new agent. You should keep
     * the builder and reuse it otherwise you'll be creating a new agent
     * for each request.
     *
     * If you have already set an agent then it will be replaced,
     * configuration will NOT be merged!
     *
     * You must call this after you set the base URL so we know whether to
     * create an http.Agent or https.Agent.
     */
    withAgentOptions(agentOptions): this {
        if (!this.config.baseUrl) {
            throw new Error('You must call .withBaseUrl() before calling .withAgentOptions()');
        }
        const urlObj = url.parse(this.config.baseUrl);
        const client = getClientForProtocol(urlObj.protocol);
        return this.withHttpOptions({
            agent: new (client.Agent as any)(agentOptions)
        });
    }

    /**
     * Set config properties.
     *
     * This can be used instead of the withXXX methods.
     */
    withConfig(config: { [K in keyof RequestConfig]: RequestConfig[K] | null }): this {
        const constructor = this.constructor as new (...args: any[]) => this;
        return new constructor(this.config, config);
    }

    /**
     * Send the request and wait for the response.
     *
     * This returns a promise which will only resolve or reject when the
     * response has finished.
     *
     * The resolved/rejected result of the promise depends on your
     * responseHandler and errorResponseHandler respectively.
     */
    send(config: RequestConfig = {}): Promise<any> {
        return executePipeline(mergeConfig(this.config, config));
    }
}
