import {NotJsonContentTypeError} from './errors';
import {ResponseHandlerOptions} from './types';

export async function defaultErrorResponseHandler(wrappedResponse) {
    let body = await wrappedResponse.bufferBody();

    // We're in error handling mode, lets try hard to give something useful.
    // If we cannot parse as JSON then we'll try to just store the response in a Buffer.
    // It may provide useful info if dumped to a log file.
    if (wrappedResponse.isContentTypeJson()) {
        try {
            body = JSON.parse(body);
        } catch (error) {
            // ignore
        }
    }

    return wrappedResponse.getStandardResponse(body);
}

export async function bufferJsonResponseHandler(wrappedResponse, opts?: ResponseHandlerOptions) {
    if (!wrappedResponse.hasBody()) {
        await wrappedResponse.discard();
        return wrappedResponse.getStandardResponse();
    }

    if (!wrappedResponse.isContentTypeJson()) {
        await wrappedResponse.discard();
        throw new NotJsonContentTypeError(wrappedResponse.getHeader('Content-Type'));
    }

    const body = await wrappedResponse.bufferJson(opts);
    return wrappedResponse.getStandardResponse(body);
}

export async function bufferBodyResponseHandler(wrappedResponse, opts?: ResponseHandlerOptions) {
    const body = await wrappedResponse.bufferBody(opts);
    return wrappedResponse.getStandardResponse(body);
}

export async function discardBodyResponseHandler(wrappedResponse) {
    await wrappedResponse.discard();
    return wrappedResponse.getStandardResponse();
}
