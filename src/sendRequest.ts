import stream = require('stream');
import {Builder} from '.';

export function sendEmptyBody(httpRequest) {
    httpRequest.end();
}

export function withJsonBody<T extends Builder>(builder: T, body: unknown): T {
    const json = JSON.stringify(body);
    const buffer = Buffer.from(json, 'utf-8');
    return builder.withConfig({
        headers: {
            'content-type': builder.config.headers['content-type'] || 'application/json',
            'content-length': buffer.length
        },
        sendRequestFn: (httpRequest) => httpRequest.end(buffer)
    });
}

export function withBufferBody<T extends Builder>(builder: T, buffer: Buffer): T {
    return builder.withConfig({
        headers: {
            'content-length': buffer.length
        },
        sendRequestFn: (httpRequest) => httpRequest.end(buffer)
    });
}

export function withStreamBody<T extends Builder>(builder: T, stream: stream.Readable): T {
    return builder.withConfig({
        sendRequestFn: (httpRequest) => {
            stream.on('error', (err) => httpRequest.emit('error', err));
            stream.pipe(httpRequest);
        }
    });
}
