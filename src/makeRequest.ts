import {WrappedResponse} from './wrappedResponse';
import {getClientForProtocol} from './utils/getClientForProtocol';

import {
    HttpleaseError,
    InvalidHttpleaseConfigError,
    UnknownHttpleaseError,
    ConnectTimeoutError,
    TimeoutError,
    UnexpectedHttpResponseCodeError
} from './errors';

function assertDefined(opts, key, msg?: string) {
    if (opts[key] === undefined || opts[key] === null) {
        throw new InvalidHttpleaseConfigError(`Missing option '${key}'${msg || ''}`);
    }
}

function assertRequiredOptionsDefined(requestConfig) {
    assertDefined(requestConfig, 'sendRequestFn');
    assertDefined(requestConfig, 'responseValidator');
    assertDefined(requestConfig, 'responseHandler');
    assertDefined(requestConfig, 'errorResponseHandler');
    assertDefined(requestConfig, 'timeout');

    ['protocol', 'hostname', 'path'].forEach((key) => {
        assertDefined(requestConfig.httpOptions, key, ' - you can set a URL with baseUrl and/or path');
    });

    assertDefined(requestConfig.httpOptions, 'method');
}

function setAbortOnTimeout(requestConfig, httpRequest) {
    httpRequest.setTimeout(requestConfig.timeout);
    httpRequest.on('timeout', () => {
        httpRequest.emit('error', new TimeoutError(requestConfig));
        httpRequest.abort();
    });
}

function setAbortOnConnectTimeout(requestConfig, httpRequest) {
    const clearConnectTimeout = createConnectTimout(requestConfig, httpRequest);

    httpRequest.once('socket', (socket) => {
        if (socket.connecting) {
            socket.once('connect', clearConnectTimeout);
        } else {
            clearConnectTimeout();
        }
    });
}

function createConnectTimout(requestConfig, httpRequest) {
    const connectTimeout = typeof requestConfig.connectTimeout === 'number'
        ? requestConfig.connectTimeout
        : requestConfig.timeout;

    const timer = setTimeout(() => {
        httpRequest.emit('error', new ConnectTimeoutError(requestConfig, connectTimeout));
        httpRequest.abort();
    }, connectTimeout);

    return function clearConnectTimeout() {
        clearTimeout(timer);
    };
}

function beginHttpRequest(requestConfig) {
    assertRequiredOptionsDefined(requestConfig);

    const client = getClientForProtocol(requestConfig.httpOptions.protocol) as any;
    const httpRequest = client.request(requestConfig.httpOptions);

    setAbortOnTimeout(requestConfig, httpRequest);
    setAbortOnConnectTimeout(requestConfig, httpRequest);

    return httpRequest;
}

function waitForResponseEnd(_httpRequest, httpResponse) {
    return new Promise<void>((resolve, reject) => {
        httpResponse.on('error', reject);
        httpResponse.on('end', () => resolve());
    });
}

function waitForResponseHandler(requestConfig, httpResponse) {
    try {
        const wrappedResponse = new WrappedResponse(httpResponse);
        if (requestConfig.responseValidator(wrappedResponse)) {
            return requestConfig.responseHandler(wrappedResponse);
        } else {
            return requestConfig.errorResponseHandler(wrappedResponse)
                .then((response) => Promise.reject(new UnexpectedHttpResponseCodeError(requestConfig, response)));
        }
    } catch (err) {
        return Promise.reject(err);
    }
}

export function makeRequest(requestConfig) {
    return new Promise((resolve, reject) => {
        const httpRequest = beginHttpRequest(requestConfig);
        httpRequest.on('error', reject);

        httpRequest.on('response', (httpResponse) => {
            Promise.all([
                waitForResponseEnd(httpRequest, httpResponse),
                waitForResponseHandler(requestConfig, httpResponse)
            ]).then(((results) => resolve(results[1])), reject);
        });

        requestConfig.sendRequestFn(httpRequest);
    }).catch((error) => { throw wrapUnknownError(error); });

    function wrapUnknownError(error) {
        if (error instanceof HttpleaseError) {
            return error;
        } else {
            return new UnknownHttpleaseError(requestConfig, error);
        }
    }
}
