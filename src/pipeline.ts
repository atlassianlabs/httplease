import {makeRequest} from './makeRequest';
import {RequestConfig} from './types';

export function executePipeline(requestConfig: RequestConfig) {
    const filters = getSortedFilters(requestConfig.filters);
    delete requestConfig.filters;

    function wrapFilter(next, filter) {
        return (requestConfig) => filter(requestConfig, next);
    }

    function handleError(error) {
        if (requestConfig.clientName) {
            error.clientName = requestConfig.clientName;
        }
        throw error;
    }

    const startFilter = filters.reduceRight(wrapFilter, makeRequest);

    return Promise.resolve(requestConfig)
        .then(startFilter)
        .catch(handleError);
}

function getSortedFilters(filters: RequestConfig['filters']) {
    return (filters || [])
        .map((filter, index) => [index, filter])
        .sort((a, b) => {
            const aIndex = a[0];
            const aOrder = a[1].order || 0;
            const bIndex = b[0];
            const bOrder = b[1].order || 0;

            if (aOrder !== bOrder) {
                return aOrder - bOrder;
            } else {
                return aIndex - bIndex;
            }
        })
        .map((tuple) => tuple[1]);
}
