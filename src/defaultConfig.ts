import * as responseHandlerHelpers from './responseHandler';
import * as sendRequestHelpers from './sendRequest';
import {RequestConfig} from './types';
import {populateHttpOptions} from './populateHttpOptions';

export const defaultConfig: RequestConfig = {
    httpOptions: {},
    filters: [populateHttpOptions],
    headers: {},
    params: {},
    sendRequestFn: sendRequestHelpers.sendEmptyBody,
    responseValidator: null,
    responseHandler: null,
    errorResponseHandler: responseHandlerHelpers.defaultErrorResponseHandler
};
